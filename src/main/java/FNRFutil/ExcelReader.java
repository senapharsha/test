package FNRFutil;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import java.util.Map;
import java.util.Map.Entry;



import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.bridj.cpp.std.list;

public class ExcelReader {

	private XSSFSheet excelWSheet;
	

	private ConfigFileReader configFileReader = new ConfigFileReader();

	public XSSFSheet getSheet(String sheetName) {
		Map<String, XSSFSheet> sheetFromExcel = new HashMap<>();
		StringBuilder newPath = new StringBuilder(configFileReader.getValue("TestData")).append("/").append("/")
				.append("TestData.xlsx");
		
		try (FileInputStream ExcelFile = new FileInputStream(newPath.toString());
				XSSFWorkbook excelWBook = new XSSFWorkbook(ExcelFile)) {

			// Access the required test data sheet

			for (int i = 0; i < excelWBook.getNumberOfSheets(); i++) {
				excelWSheet = excelWBook.getSheetAt(i);
				sheetFromExcel.put(excelWBook.getSheetAt(i).getSheetName(), excelWBook.getSheetAt(i));
			}
			for (Entry<String, XSSFSheet> entry : sheetFromExcel.entrySet()) {
				if (sheetName.equals(entry.getKey())) {
					excelWSheet = entry.getValue();
				}
			}
		} catch (IOException e) {
			Log.debug("Error message:" + e);
		}

		return excelWSheet;
	}

	public Map<String, String> getHashValueFromExcel(XSSFSheet excelWSheet, int row)

	{
		Log.debug("enter");
		Map<String, String> totHash = new HashMap<>();
		try {
			

				
					
						for (int j = 0; j <= excelWSheet.getRow(row).getLastCellNum() - 1; j++) {
							String keyForMap = "";
							keyForMap = excelWSheet.getRow(0).getCell(j).getStringCellValue().trim();
							if (excelWSheet.getRow(row).getCell(j) != null
									&& excelWSheet.getRow(row).getCell(j).getCellTypeEnum() != CellType.BLANK
									&& excelWSheet.getRow(row).getCell(j).getCellTypeEnum() == CellType.STRING) {
								totHash.put(keyForMap, excelWSheet.getRow(row).getCell(j).getStringCellValue().trim());
							} else if (excelWSheet.getRow(row).getCell(j) != null
									&& excelWSheet.getRow(row).getCell(j).getCellTypeEnum() != CellType.BLANK
									&& excelWSheet.getRow(row).getCell(j).getCellTypeEnum() == CellType.NUMERIC) {

								totHash.put(keyForMap, NumberToTextConverter
										.toText(excelWSheet.getRow(row).getCell(j).getNumericCellValue()));
							}
						}
						
					
				

			
		} catch (Exception e) {
			Log.info("Exception" + e);
		} 
		return totHash;
	}

	
	public String[][] getSateID(XSSFSheet excelWSheet) {
		int num=excelWSheet.getLastRowNum();
		int j=0;
		String[][] list=new String[num][num];
		try {
			for (int i = 1; i <= excelWSheet.getLastRowNum(); i++) {
				if (excelWSheet.getRow(i).getCell(j) != null
						&& excelWSheet.getRow(i).getCell(j).getCellTypeEnum() != CellType.BLANK
						&& excelWSheet.getRow(i).getCell(j).getCellTypeEnum() == CellType.STRING) {
					String testcaseId= excelWSheet.getRow(i).getCell(j).getStringCellValue().trim();
					
						list[i-1][j]=testcaseId;
						System.out.println(testcaseId);
					
					
								} 
				

			}
		}catch(Exception e) {
			Log.info("Exception" + e);
		}
		return list;
	}
	
	
	
public ArrayList<Integer> getCellRowNum(String StateId,XSSFSheet excelWSheet){
	int row = 0;
	ArrayList<Integer> l=new ArrayList<Integer>();
	try {
		for (int i = 0; i <= excelWSheet.getLastRowNum(); i++) {

			if (StateId.equalsIgnoreCase(excelWSheet.getRow(i).getCell(0).getStringCellValue().trim())) {
				
				row=i;	
				l.add(row);
				
			}

		}
	} catch (Exception e) {
		Log.info("Exception" + e);
	} 
	return l;
	}


//returns the row count in a sheet
	public static int getRowCount(XSSFSheet excelWSheet){
		
		int number=excelWSheet.getLastRowNum()+1;
		return number;
		
		
	}

	
	// returns number of columns in a sheet	
		public static int getColumnCount(XSSFSheet excelWSheet){
			
			return excelWSheet.getRow(0).getLastCellNum();
			
		
		}

public static Object[][] getData(XSSFSheet sheetName){

	int rows=getRowCount(sheetName);
	int cols=getColumnCount(sheetName);
		
    Object[][] data =new Object[rows-1][cols-3];
	for(int rowNum=2;rowNum<=rows;rowNum++){
		for(int colNum=0;colNum<cols-3;colNum++){
			//System.out.print(xls.getCellData(testCaseName, colNum, rowNum) + " -- ");
			data[rowNum-2][colNum] = getCellData(sheetName, colNum, rowNum);
		}
		//System.out.println();
	}
	return data;
	
}



//returns the data from a cell
	public static String getCellData(XSSFSheet sheetName,int colNum,int rowNum){
		
		XSSFRow row = sheetName.getRow(rowNum-1);
		XSSFCell cell = row.getCell(colNum);
	 
		return cell.getStringCellValue();
	}









}
