package FNRFutil;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.ui.Select;

import FRNF.Pages.LandingPage;

public class ReusableMethods  {

	

	private XSSFSheet excelWSheet = null;
	private ExcelReader excelReader = new ExcelReader();
	public int i = 1;
	public static int upload=1;
	//public static String mainWindow;

	public static void enterTextByClearValue(WebElement ele, String text, String eleName, WebDriver driver) {
		try {
			Wait.waitUnitWebElementVisible(driver, ele);
			if (ele.isDisplayed()) {
				ele.clear();
				if (text != null) {
					ele.sendKeys(text);
				}
			}
		} catch (Exception e) {
			Log.debug(eleName + " Is not present" + ": EXception Error : " + e);
		}
	}

	public static void click(WebElement ele, String eleName, WebDriver driver) {
		try {
			Wait.waitUnitWebElementVisible(driver, ele);
			if (ele.isDisplayed()) {
				ele.click();
				Log.debug(eleName + " is  present");
			}
		} catch (Exception e) {
			Log.debug(eleName + " is Not present" + ": EXCEption Error : " + e);
		}
	}

	public String getElementValueByText(WebElement webelement, String webelementName, WebDriver driver) {

		String elementValue = null;
		try {
			Wait.waitUnitWebElementVisible(driver, webelement);
			elementValue = webelement.getText().trim();
		} catch (Exception e) {
			Log.debug(webelementName + "Not PREsent" + ": ExceptiOn Error : " + e);
		}
		return elementValue;
	}

	public void Checkelementclick(WebElement webelement, String webelementName, WebDriver driver) {

		for (int i = 1; i <= 2; i++) {
			javascriptclick(webelement, driver);

		}

	}
	public String switchWindow(WebDriver driver, String mainWindow) {
	//String mainWindow=driver.getWindowHandle();
	Set<String> set =driver.getWindowHandles();
	String childWindow = null;
	Iterator<String> itr= set.iterator();
	while(itr.hasNext()){
		childWindow=itr.next();
	   	
		if(!mainWindow.equals(childWindow)){
		driver.switchTo().window(childWindow);
		
		break;
		}
	}
	
	return childWindow;
	}
	
	
	
	public String switchToThirdWindow(WebDriver driver, String credWindow, String subWindow) {
		
		Set<String> set =driver.getWindowHandles();
		
		String babyWindow = null;
		Iterator<String> itr= set.iterator();
		while(itr.hasNext()){
			babyWindow=itr.next();
		   	
			if(!credWindow.equals(babyWindow) && !subWindow.equals(babyWindow) ){
			driver.switchTo().window(babyWindow);
			
			break;
			}
		}
		
		return babyWindow;
		}
	

	public void selectUsingValue(WebElement element, String value) {
		Select select = new Select(element);
		Log.debug("selectUsingValue and value is: " + value);
		select.selectByValue(value);
	}

	public Map<String, String> getInputData(String InputDataSheetName, int row) {
		excelWSheet = excelReader.getSheet(InputDataSheetName);
		return excelReader.getHashValueFromExcel(excelWSheet, row);
	}

	public Object[][] getData(String InputDataSheetName) {
		excelWSheet = excelReader.getSheet(InputDataSheetName);
		return excelReader.getSateID(excelWSheet);
	}
	
	public ArrayList<Integer> getrow(String State,String InputDataSheetName) {
		excelWSheet = excelReader.getSheet(InputDataSheetName);
		return excelReader.getCellRowNum(State,excelWSheet);
	}

	
	public void CheckBtn_click(WebElement ele, WebDriver driver, String flag, String eleName) {
		try {
		if(flag.equals("Y")) {
			sleep(1000);
			ele.click();
			
						
		}
		}catch (Exception e) {
			Log.debug(eleName + " Is not present" + ": EXception Error : " + e);
		}
		
	}
	
	public void highlightenter(WebElement ele, WebDriver driver, String text, String eleName) {
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		jsExecutor.executeScript("arguments[0].style.border='2px solid Yellow'", ele);
		enterTextByClearValue(ele, text, eleName, driver);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	
	
	
	
	public static void selectState(WebElement ele, WebDriver driver, String text, String eleName) {
		
		ele.sendKeys(text);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ele.sendKeys(Keys.ENTER);
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ele.sendKeys(text);
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);
		
	}
	
	public void sleep(int time) {
	
	try {
		Thread.sleep(time);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}

	public void javascriptclick(WebElement ele, WebDriver driver) {
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);
	}

	public void setClipboardData(String string) {
		// StringSelection is a class that can be used for copy and paste operatio
		StringSelection stringSelection1 = new StringSelection("");

		System.out.println(stringSelection1);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection1, null);
		StringSelection stringSelection = new StringSelection(string);

		System.out.println(stringSelection);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
		System.out.println(stringSelection);

	}

	public void uploadFile(String fileLocation) {
		try {
			if (i == 1 ) {
				setClipboardData(fileLocation);

				// native key strokes for CTRL, V and ENTER keys
				Thread.sleep(5000);
				Robot robot = new Robot();
				
				robot.keyPress(KeyEvent.VK_CONTROL);
				System.out.println("VK_CONTROL");

				Thread.sleep(2000);

				robot.keyPress(KeyEvent.VK_V);
				System.out.println("v");

				Thread.sleep(2000);

				robot.keyRelease(KeyEvent.VK_CONTROL);

				Thread.sleep(2000);

				robot.keyRelease(KeyEvent.VK_V);
				System.out.println("rel v");

				Thread.sleep(2000);

				robot.keyPress(KeyEvent.VK_ENTER);
				System.out.println("Enter");

				Thread.sleep(2000);

				robot.keyRelease(KeyEvent.VK_ENTER);
				i++;
			} else  {
				setClipboardData(fileLocation);

				// native key strokes for CTRL, V and ENTER keys
				Thread.sleep(3000);
				Robot robot = new Robot();

				robot.keyPress(KeyEvent.VK_CONTROL);
				System.out.println("VK_CONTROL");

				Thread.sleep(1000);

				robot.keyPress(KeyEvent.VK_V);
				System.out.println("v");

				Thread.sleep(1000);

				robot.keyRelease(KeyEvent.VK_CONTROL);

				Thread.sleep(1000);

				robot.keyRelease(KeyEvent.VK_V);
				System.out.println("rel v");

				Thread.sleep(1000);
				
				robot.keyPress(KeyEvent.VK_ENTER);
				System.out.println("Enter");

				Thread.sleep(1000);

				robot.keyRelease(KeyEvent.VK_ENTER);
				Thread.sleep(1000);
				
				robot.keyPress(KeyEvent.VK_ALT);
				System.out.println("VK_ALT--2");
				Thread.sleep(1000);
				robot.keyPress(KeyEvent.VK_N);
				Thread.sleep(1000);
				robot.keyRelease(KeyEvent.VK_N);
				Thread.sleep(1000);
				robot.keyRelease(KeyEvent.VK_ALT);
				Thread.sleep(1000);

				robot.keyPress(KeyEvent.VK_CONTROL);
				System.out.println("VK_CONTROL");

				Thread.sleep(1000);

				robot.keyPress(KeyEvent.VK_V);
				System.out.println("v");

				Thread.sleep(1000);

				robot.keyRelease(KeyEvent.VK_CONTROL);

				Thread.sleep(1000);

				robot.keyRelease(KeyEvent.VK_V);
				System.out.println("rel v");
				Thread.sleep(1000);
				robot.keyPress(KeyEvent.VK_ENTER);
				System.out.println("Enter");

				Thread.sleep(1000);

				robot.keyRelease(KeyEvent.VK_ENTER);
				
			}

		} catch (Exception exp) {

			exp.printStackTrace();

		}

	}
	
	
	public void uploadFileForStaffing(String fileLocation) {
		try {
				setClipboardData(fileLocation);

				// native key strokes for CTRL, V and ENTER keys
				Thread.sleep(5000);
				Robot robot = new Robot();
				
				robot.keyPress(KeyEvent.VK_CONTROL);
				System.out.println("VK_CONTROL");

				Thread.sleep(2000);

				robot.keyPress(KeyEvent.VK_V);
				System.out.println("v");

				Thread.sleep(2000);

				robot.keyRelease(KeyEvent.VK_CONTROL);

				Thread.sleep(2000);

				robot.keyRelease(KeyEvent.VK_V);
				System.out.println("rel v");

				Thread.sleep(2000);

				robot.keyPress(KeyEvent.VK_ENTER);
				System.out.println("Enter");

				Thread.sleep(2000);

				robot.keyRelease(KeyEvent.VK_ENTER);
								Thread.sleep(1000);
				
				robot.keyPress(KeyEvent.VK_ALT);
				System.out.println("VK_ALT--2");
				Thread.sleep(1000);
				robot.keyPress(KeyEvent.VK_N);
				Thread.sleep(1000);
				robot.keyRelease(KeyEvent.VK_N);
				Thread.sleep(1000);
				robot.keyRelease(KeyEvent.VK_ALT);
				Thread.sleep(1000);

				robot.keyPress(KeyEvent.VK_CONTROL);
				System.out.println("VK_CONTROL");

				Thread.sleep(1000);

				robot.keyPress(KeyEvent.VK_V);
				System.out.println("v");

				Thread.sleep(1000);

				robot.keyRelease(KeyEvent.VK_CONTROL);

				Thread.sleep(1000);

				robot.keyRelease(KeyEvent.VK_V);
				System.out.println("rel v");
				Thread.sleep(1000);
				robot.keyPress(KeyEvent.VK_ENTER);
				System.out.println("Enter");

				Thread.sleep(1000);

				robot.keyRelease(KeyEvent.VK_ENTER);
				
				
				
			}

		catch (Exception exp) {

			exp.printStackTrace();

		}

	}


	public void clickdropdown(int place) {

		try {
			Robot robot = new Robot();
			for (int i = 0; i < place; i++) {
				Thread.sleep(1500);

				robot.keyPress(KeyEvent.VK_DOWN);
				Thread.sleep(1500);
				robot.keyRelease(KeyEvent.VK_DOWN);

			}
			Thread.sleep(1500);
			robot.keyPress(KeyEvent.VK_ENTER);
			Thread.sleep(1500);
			robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
    

    


	

}
