package FRNF.Pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.sikuli.script.Key;

import FNRFutil.ConfigFileReader;
import FNRFutil.ReusableMethods;
import FNRFutil.Wait;

public class AcknowledgeandCertify {
	WebDriver driver;
	ConfigFileReader configFileReader = null;
	ReusableMethods ReusableMethods = null;

	public AcknowledgeandCertify(WebDriver driver) {
		this.driver = driver;
		configFileReader = new ConfigFileReader();
		ReusableMethods = new ReusableMethods();
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//*[text()='Acknowledge and Certify']//following::input[1]")
	private WebElement Acknowledge_CHCK;
	
	@FindBy(xpath = "//*[text()='Attesting Individual Name (Submitter)']//following::input[1]")
	private WebElement Attesting;
	
	@FindBy(xpath = "//*[text()='Submitter Title']//following::input[1]")
	private WebElement Submitter;
	
	@FindBy(xpath = "//*[text()='Date']//following::input[1]")
	private WebElement Date;
	
	@FindBy(xpath = "//button[text()='Save & Continue']")
	private WebElement SaveCountinue_btn;
	
	public void doaccept() {
		try {
			Thread.sleep(2500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Wait.waitUnitWebElementVisible(driver, Acknowledge_CHCK);
		ReusableMethods.javascriptclick(Acknowledge_CHCK, driver);
		Attesting.sendKeys("Anil");
		Submitter.sendKeys("Surgeon");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

//		//ReusableMethods.javascriptclick(Date, driver);
//		Date.clear();
//		Date.sendKeys("Sep 14, 2021");
//		
//		
//		try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		Date.clear();
//		Date.sendKeys("Sep 14, 2021");
//		
//		try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
		Robot r;
		try {
			r = new Robot();
			
			r.keyPress(KeyEvent.VK_TAB);
			Thread.sleep(1000);
			r.keyRelease(KeyEvent.VK_TAB);
			Thread.sleep(1000);
			r.keyPress(KeyEvent.VK_TAB);
			Thread.sleep(1000);
			r.keyRelease(KeyEvent.VK_TAB);
			Thread.sleep(1000);
			r.keyPress(KeyEvent.VK_ENTER);
			Thread.sleep(1000);
			r.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(1000);
			
			r.keyPress(KeyEvent.VK_ENTER);
			Thread.sleep(1000);
			r.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(1000);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		ReusableMethods.javascriptclick(SaveCountinue_btn, driver);
	}
}
