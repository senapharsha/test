package FRNF.Pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import FNRFutil.ConfigFileReader;
import FNRFutil.ReusableMethods;
import FNRFutil.Wait;

public class Compensation {

	WebDriver driver;
	ConfigFileReader configFileReader = null;
	ReusableMethods ReusableMethods = null;

	public Compensation(WebDriver driver) {
		this.driver = driver;
		configFileReader = new ConfigFileReader();
		ReusableMethods = new ReusableMethods();
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//*[text()='I/P Locked - Retail Rate for Adult']//following::input[1]")
	private WebElement IPLockedRetailRateforAdult_TXT;
	
	@FindBy(xpath = "//button[text()='Save & Continue']")
	private WebElement SaveCountinue_btn;
	
	@FindBy(xpath = "//*[text()='ECT - Retail Rate for Child Outpatient']//following::input[1]")
	private WebElement ECTRetailRateforChildOutpatient_TXT;
	
	@FindBy(xpath = "//*[text()='ECT - Discount Rate for Child Outpatient']//following::input[1]")
	private WebElement ECDiscountRateforChildOutpatient_TXT;
	
	public void compenstion() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		List<WebElement> list = driver.findElements(By.xpath("//input[@class='slds-input']"));
		for (int i = 0; i < list.size(); i++) {
			WebElement ele = list.get(i);
			ele.sendKeys("1");
		

	}
		ReusableMethods.javascriptclick(SaveCountinue_btn, driver);
}
}
