package FRNF.Pages;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import FNRFutil.ConfigFileReader;
import FNRFutil.ReusableMethods;
import FNRFutil.Wait;

public class ContractorPage {
	WebDriver driver;
	ConfigFileReader configFileReader = null;
	ReusableMethods ReusableMethods = null;

	public ContractorPage(WebDriver driver) {
		this.driver = driver;
		configFileReader = new ConfigFileReader();
		ReusableMethods = new ReusableMethods();
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//*[text()='Is your Facility currently contracted with Optum?']//following::button[1]")
	private WebElement Contractor_SEL;
	
	@FindBy(xpath = "//*[text()='Facility Legal Name']//following::input[1]")
	private WebElement FacLegalName_TXT;
	
	@FindBy(xpath = "//*[text()='Credentialing Contact Email']//following::input[1]")
	private WebElement CredCtcMail_TXT;
	
	@FindBy(xpath = "//button[text()='Save & Continue']")
	private WebElement SaveCountinue_btn;
	
	@FindBy(xpath = "//*[text()='No']")
	private WebElement No_btn;
	
	public void setcontractorstatus(Map<String, String> xl) {
		
		Wait.waitUnitWebElementVisible(driver, Contractor_SEL);
		ReusableMethods.highlightenter(FacLegalName_TXT,driver,xl.get("FacilityLegalName"),"Facility name");
		ReusableMethods.highlightenter(CredCtcMail_TXT,driver,xl.get("CredentialingContactEmail"),"Cred Contact Email");
		
		
		Wait.waitUnitWebElementVisible(driver, Contractor_SEL);
		ReusableMethods.javascriptclick(Contractor_SEL, driver);
		
		Wait.waitUnitWebElementVisible(driver, No_btn);
		ReusableMethods.javascriptclick(No_btn, driver);
		
		
		
		String s1=Contractor_SEL.getText();
		System.out.println(s1);
		
		SaveCountinue_btn.click();
	}
}
