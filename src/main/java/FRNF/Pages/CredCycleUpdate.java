package FRNF.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import FNRFutil.ConfigFileReader;
import FNRFutil.ReusableMethods;

public class CredCycleUpdate {
	
	
	WebDriver driver;
	ConfigFileReader configFileReader = null;
	ReusableMethods ReusableMethods = null;
	public static String credWindow;

	public CredCycleUpdate(WebDriver driver) {
		this.driver = driver;
		configFileReader = new ConfigFileReader();
		ReusableMethods = new ReusableMethods();
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(xpath = "//input[@title='New License/CDS/DEA']")
	public static WebElement NewLicense_BTN;
	
	@FindBy(xpath = "//label[text()='PSV Applicable']/following::select[1]")
	public static WebElement PsvApplicable_select;
	
	@FindBy(xpath = "//label[text()='State']/following::select[1]")
	public static WebElement State_select;
	
	@FindBy(xpath = "//label[text()='Issue Date']/following::input[1]")
	public static WebElement IssueDate_TXT;
	
	@FindBy(xpath = "//label[text()='Issue Date']/following::a[1]")
	public static WebElement IssueDate_LNK;
	
	@FindBy(xpath = "//label[text()='Expiration Date']/following::input[1]")
	public static WebElement ExpirationDate_TXT;
	
	@FindBy(xpath = "//label[text()='Expiration Date']/following::a[1]")
	public static WebElement ExpirationDate_LNK;
	
	@FindBy(xpath = "//label[text()='Verified Date']/following::input[1]")
	public static WebElement VerifiedDate_TXT;
	
	@FindBy(xpath = "//label[text()='Verified Date']/following::a[1]")
	public static WebElement VerifiedDate_LNK;
	
	@FindBy(xpath = "//td[@id='bottomButtonRow']/descendant::input[@name='save']")
	public static WebElement Save_BTN;
	
	@FindBy(xpath = "//input[@title='New Cred Exception']")
	public static WebElement NewCredException_BTN;
	
	@FindBy(xpath = "//label[text()='Exceptions Processor']/following::input[1]")     //Lisa Riddle
	public static WebElement ExceptionsProcessor_TXT;
	
	@FindBy(xpath = "//label[text()='License Issue Date']/following::a[1]")     
	public static WebElement LicenseIssueDate_LNK;
	
	@FindBy(xpath = "//label[text()='File Expiration Date']/following::input[1]")     
	public static WebElement FileExpirationDate_TXT;
	
	@FindBy(xpath = "//label[text()='File Expiration Date']/following::a[1]")     
	public static WebElement FileExpirationDate_LNK;
	
	@FindBy(xpath = "//input[@title='New Accreditation']")     
	public static WebElement NewAccreditation_BTN;
	
	@FindBy(xpath = "//label[text()='Accrediting Body']/following::select[1]")
	public static WebElement AccreditingBody_select;
	
	@FindBy(xpath = "//label[text()='Source']/following::select[1]")
	public static WebElement Source_select;
	
	@FindBy(xpath = "//label[text()='Status']/following::select[1]")
	public static WebElement Status_select;
	
	@FindBy(xpath = "//label[text()='Effective Date']/following::a[1]")
	public static WebElement EffectiveDate_LNK;
	
	
	
	@FindBy(xpath = "//input[@title='New Insurance']")     
	public static WebElement NewInsurance_BTN;
	
	@FindBy(xpath = "//label[text()='Insurance Type']/following::select[1]")
	public static WebElement InsuranceType_select;
	
	@FindBy(xpath = "//label[text()='Carrier']/following::input[1]")
	public static WebElement Carrier_TXT;
	
	@FindBy(xpath = "//input[@title='New Board Certification']")     
	public static WebElement NewBoardCertification_BTN;
	
	
	@FindBy(xpath = "//a[@title='Board Certification Type Lookup (New Window)']")     //AANP Amer Acad of Nurse Pract
	public static WebElement BoardCertificationTypeLookup_LNK;
	
	@FindBy(xpath = "//a[@title='Board Certification Source Lookup (New Window)']")     //American Board of Psychiatry and Neurology	
	public static WebElement BoardCertificationSource_LNK;  
	
	@FindBy(xpath = "//label[text()='Board Certification Expiration Date']/following::input[1]")
	public static WebElement BoardCertificationExpirationDate_TXT;
	
	@FindBy(xpath = "//label[text()='Board Certification Expiration Date']/following::a[1]")
	public static WebElement BoardCertificationExpirationDate_LNK;
	
	@FindBy(xpath = "//label[text()='Board Certification Verified Date']/following::a[1]")
	public static WebElement BoardCertificationVerifiedDate_LNK;
	
	@FindBy(xpath = "//label[text()='Received From Network Date']/following::a[1]")     
	public static WebElement ReceivedFromNetworkDate_LNK;
	
	@FindBy(xpath = "//label[text()='App Received Date']/following::a[1]")     
	public static WebElement AppReceivedDate_LNK;
	
	@FindBy(xpath = "//label[text()='Complete Application Received Date']/following::a[1]")     
	public static WebElement CompleteApplicationReceivedDate_LNK;
	
	@FindBy(xpath = "//label[text()='Attestation Signature Date']/following::a[1]")     
	public static WebElement AttestationSignatureDate_LNK;
	
	@FindBy(xpath = "//label[text()='Cycle Status']/following::select[1]")
	public static WebElement CycleStatus_select;
	
	@FindBy(xpath = "//label[text()='NPDB Verified Date']/following::a[1]")     
	public static WebElement NPDBVerifiedDate_LNK;
	
	@FindBy(xpath = "//label[text()='Other Sanction Applicable']/following::select[1]")
	public static WebElement OtherSanctionApplicable_select;
	
	@FindBy(xpath = "//label[text()='NPPES Applicable']/following::select[1]")
	public static WebElement NPPESApplicable_select;
	
	@FindBy(xpath = "//label[text()='EPLS/SAM Sanction Verified Date']/following::a[1]")     
	public static WebElement PlsSamSanctionVerifiedDate_LNK;
	
	@FindBy(xpath = "//label[text()='W-9 Attached']/following::input[1]")     
	public static WebElement W9Attached_chk;
	
	@FindBy(xpath = "//label[text()='Policies Applicable']/following::select[1]")
	public static WebElement PoliciesApplicable_select;
	
	@FindBy(xpath = "//td[@id='topButtonRow']//input[@title='Edit']")     
	public static WebElement Edit_BTN;
	
	
	@FindBy(xpath = "//input[@id='lksrch']")     
	public static WebElement LookupSearch_TXT;
	
	@FindBy(xpath = "//input[@title='Go!']")     
	public static WebElement LookupGo_BTN;
	
	
	@FindBy(xpath = "//a[contains(@class,'dataCell')]")     
	public static WebElement LookupResult_LNK;
	
	@FindBy(xpath = "//a[@title='Exceptions Processor Lookup (New Window)']")
	public static WebElement Lookup_BTN;
	
	public void searchAndClickInLookUpWin(String str) {
		driver.switchTo().frame("searchFrame");
		LookupSearch_TXT.sendKeys(str);
		LookupGo_BTN.click();
		
		driver.switchTo().defaultContent();
		driver.switchTo().parentFrame();
		driver.switchTo().frame("resultsFrame");
		
		LookupResult_LNK.click();
		
		
		
	}
	
	public void CredUpdate(String state) {
		
		ReusableMethods.sleep(15000);
		Edit_BTN.click();
		ReusableMethods.sleep(4000);
		
		ReusableMethods.selectUsingValue(CycleStatus_select, "Medical Director Approval");
		ReceivedFromNetworkDate_LNK.click();
		AppReceivedDate_LNK.click();
		CompleteApplicationReceivedDate_LNK.click();
		AttestationSignatureDate_LNK.click();
		
		NPDBVerifiedDate_LNK.click();
		ReusableMethods.sleep(2000);
		ReusableMethods.selectUsingValue(OtherSanctionApplicable_select, "No");
		ReusableMethods.sleep(2000);
		ReusableMethods.selectUsingValue(NPPESApplicable_select, "No");
		ReusableMethods.sleep(2000);
		ReusableMethods.selectUsingValue(PoliciesApplicable_select, "No");
		ReusableMethods.sleep(2000);

		PlsSamSanctionVerifiedDate_LNK.click();
		//ReusableMethods.sleep(1000);
		//ReusableMethods.javascriptclick(W9Attached_chk, driver);
		W9Attached_chk.click();
		
		ReusableMethods.sleep(2000);
		
		Save_BTN.click();
		
		ReusableMethods.sleep(15000);
		
		credWindow=driver.getWindowHandle();
		System.out.println("Main Window : "+driver.getTitle());
		
		NewCredException_BTN.click();
		
		ReusableMethods.sleep(5000);
		
		String subWindow=ReusableMethods.switchWindow(driver, credWindow);
		Lookup_BTN.click();
		String babyWindow=ReusableMethods.switchToThirdWindow(driver, credWindow, subWindow);
		searchAndClickInLookUpWin("Lisa Riddle");
		
		
		driver.switchTo().window(subWindow);
		System.out.println("sub Window: "+driver.getTitle());
		
		LicenseIssueDate_LNK.click();
		FileExpirationDate_TXT.sendKeys("10/10/2022");
	
		
		Save_BTN.click();
		ReusableMethods.sleep(3000);
		
		driver.close();
		driver.switchTo().window(credWindow);

		/*--------------------------------------------------------------------------*/
		
		NewLicense_BTN.click();
		
ReusableMethods.sleep(3000);
		
		subWindow=ReusableMethods.switchWindow(driver, credWindow);
		ReusableMethods.sleep(3000);
		ReusableMethods.selectUsingValue(PsvApplicable_select, "No");
		ReusableMethods.selectUsingValue(State_select   , "ID");
		
		IssueDate_LNK.click();
		//ExpirationDate_TXT.sendKeys("10/10/2022");
		ExpirationDate_LNK.click();
		ExpirationDate_TXT.sendKeys(Keys.BACK_SPACE);
		ExpirationDate_TXT.sendKeys(Keys.NUMPAD3);
		VerifiedDate_LNK.click();
		
		
		Save_BTN.click();
		ReusableMethods.sleep(3000);
				
		driver.close();
		driver.switchTo().window(credWindow);

			/*---------------------------------------------------------------*/
		
		ReusableMethods.sleep(5000);
		
		
		
		driver.findElement(By.xpath("//a[contains(@id,'00N80000005AgpS_link')]//span[@class='listTitle']")).click();
		//a[@id='a0b2D000003fNr8_00N80000005AgpS_link']//span[@class='listTitle']
		ReusableMethods.sleep(3000);
		NewAccreditation_BTN.click();
		
		ReusableMethods.sleep(3000);
				
				subWindow=ReusableMethods.switchWindow(driver, credWindow);
				ReusableMethods.selectUsingValue(AccreditingBody_select, "The Joint Commission");
				ReusableMethods.selectUsingValue(Source_select   , "Website");
				ReusableMethods.selectUsingValue(Status_select   , "Conditional");
				
				EffectiveDate_LNK.click();
				//ExpirationDate_TXT.sendKeys("10/10/2022");
				ExpirationDate_LNK.click();
				ExpirationDate_TXT.sendKeys(Keys.BACK_SPACE);
				ExpirationDate_TXT.sendKeys(Keys.NUMPAD3);
				
				VerifiedDate_LNK.click();
				
				
				Save_BTN.click();
				ReusableMethods.sleep(3000);
						
				driver.close();
				driver.switchTo().window(credWindow);
				
				/*------------------------------------------------------------------------*/
				
				ReusableMethods.sleep(5000);
				driver.findElement(By.xpath("//a[contains(@id,'00N80000005AgpX_link')]//span[@class='listTitle']")).click();
				ReusableMethods.sleep(3000);
				
				NewInsurance_BTN.click();
				
				ReusableMethods.sleep(3000);
						
						subWindow=ReusableMethods.switchWindow(driver, credWindow);
						ReusableMethods.selectUsingValue(InsuranceType_select, "General Liability");
						ReusableMethods.selectUsingValue(State_select   , "ID");
						//ReusableMethods.selectUsingValue(Carrier_TXT   , "INSURANCE");
						Carrier_TXT.sendKeys("INSURANCE");
						IssueDate_LNK.click();
						//ExpirationDate_TXT.sendKeys("10/10/2022");
						ExpirationDate_LNK.click();
						ExpirationDate_TXT.sendKeys(Keys.BACK_SPACE);
						ExpirationDate_TXT.sendKeys(Keys.NUMPAD3);
						VerifiedDate_LNK.click();
						
						
						Save_BTN.click();
						ReusableMethods.sleep(3000);
								
						driver.close();
						driver.switchTo().window(credWindow);
						
						/*--------------------------------------------------------------*/ 
						ReusableMethods.sleep(5000);
						//driver.findElement(By.linkText("Board Certifications")).click(); 
						driver.findElement(By.xpath("//a[contains(@id,'00N80000005AIFM_link')]//span[@class='listTitle']")).click();
						ReusableMethods.sleep(3000);
						NewBoardCertification_BTN.click();
						
						ReusableMethods.sleep(5000);
						
						subWindow=ReusableMethods.switchWindow(driver, credWindow);
						
						BoardCertificationTypeLookup_LNK.click();
										
								
						babyWindow=ReusableMethods.switchToThirdWindow(driver, credWindow, subWindow);
						searchAndClickInLookUpWin("AANP Amer Acad of Nurse Pract");
						
						
						driver.switchTo().window(subWindow);
						System.out.println("sub Window: "+driver.getTitle());
						
						ReusableMethods.sleep(4000);
								
						BoardCertificationSource_LNK.click();
						
						babyWindow=ReusableMethods.switchToThirdWindow(driver, credWindow, subWindow);
						searchAndClickInLookUpWin("American Board of Psychiatry and Neurology");
						
						
						driver.switchTo().window(subWindow);
						System.out.println("sub Window: "+driver.getTitle());
						
						IssueDate_LNK.click();
						BoardCertificationExpirationDate_TXT.sendKeys("10/10/2022");
						
						BoardCertificationExpirationDate_LNK.click();
						BoardCertificationExpirationDate_TXT.sendKeys(Keys.BACK_SPACE);
						BoardCertificationExpirationDate_TXT.sendKeys(Keys.NUMPAD3);
						
						BoardCertificationVerifiedDate_LNK.click();
								
								Save_BTN.click();
								ReusableMethods.sleep(3000);
										
								driver.close();
								driver.switchTo().window(credWindow);
								
								
				/*---------------------------------------------------------------------------
				 * 
				 */
			
								
		
		
		
		
		
	}
	

}
