package FRNF.Pages;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import FNRFutil.ConfigFileReader;
import FNRFutil.ReusableMethods;
import FNRFutil.Wait;

public class Deliveryofcare {
	WebDriver driver;
	ConfigFileReader configFileReader = null;
	ReusableMethods ReusableMethods = null;

	public Deliveryofcare(WebDriver driver) {
		this.driver = driver;
		configFileReader = new ConfigFileReader();
		ReusableMethods = new ReusableMethods();
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//*[text()='How often is individual therapy provided?']//following::input[1]")
	private WebElement individualtherapy;

	@FindBy(xpath = "//*[text()='How often is family therapy provided?']//following::input[1]")
	private WebElement familytherapy;

	@FindBy(xpath = "//*[text()='What is the patient to staff ratio?']//following::input[1]")
	private WebElement staffratio;

	@FindBy(xpath = "//*[text()='What is the staff position responsible for discharge planning?']//following::input[1]")
	private WebElement dischargeplanning;

	@FindBy(xpath = "//*[text()='Describe your discharge planning procedures']//following::input[1]")
	private WebElement planningprocedures;

	@FindBy(xpath = "//*[text()='What percentage of patients are referred for follow up care?']//following::input[1]")
	private WebElement followupcare;

	@FindBy(xpath = "//*[text()='What are your protocols for psych testing?']//following::input[1]")
	private WebElement psychtesting;
	
	@FindBy(xpath = "//*[text()='What percentage of patients are directly admitted to the partial and IOP programs?']//following::input[@type='text' and @inputmode='decimal']")
	private List<WebElement> ALOS;

	@FindBy(xpath = "//*[text()='For the partial hospital and IOP services, does the program serve as a step down or are patients directly admitted?']//following::input[1]")
	private WebElement IOPservices;

	@FindBy(xpath = "//*[text()='What percentage of patients are directly admitted to the partial and IOP programs?']/preceding::button[1]")
	private WebElement PartialHospital;

	@FindBy(xpath = "//*[text()='What percentage of patients are directly admitted to the partial and IOP programs?']//following::input[1]")
	private WebElement percentageofpatients;
	
	@FindBy(xpath = "//*[text()='What percentage of patients are directly admitted to the partial and IOP programs?']/following::button[1]")
	private WebElement externalorganizations;

	@FindBy(xpath = "//*[text()='What percentage of patients are directly admitted to the partial and IOP programs?']//following::input[@role='combobox']")
	private WebElement externalorganizations_1;

	@FindBy(xpath = "//*[text()='Policy and Procedure on Intake/Access Process to Behavioral Medicine']//following::label[1]")
	private WebElement BehavioralMedicine;

	@FindBy(xpath = "//*[text()='Policy and Procedure on Intake/Access Process if done through E.R']//following::label[1]")
	private WebElement ER;

	@FindBy(xpath = "//*[text()='Policy and Procedure on Holds/Restraints']//following::label[1]")
	private WebElement HoldsRestraints;

	@FindBy(xpath = "//*[text()='Policy and Procedure for Discharge Planning']//following::label[1]")
	private WebElement DischargePlanning;

	@FindBy(xpath = "//a[text()='Delivery of Care']")
	private WebElement Locations_lnk;

	@FindBy(xpath = "/html/body/div[7]/div/div[2]/div/div[3]/div/span[2]/div/button")
	private WebElement Done_btn1;

	
	@FindBy(xpath = "//button[text()='Save & Continue']")
	private WebElement SaveCountinue_btn;
	

	@FindBy(xpath = "//*[text()='Locked']//following::input[1]")
	private WebElement Locked;
	
	
	
	@FindBy(xpath = "//*[text()='Acute']//following::input[1]")
	private WebElement Acute;
	
	
	@FindBy(xpath = "//*[text()='Residential']//following::input[1]")
	private WebElement Residential;
	
	
	@FindBy(xpath = "//*[text()='Partial Hospitalization']//following::input[1]")
	private WebElement PartialHospitalization;
	
	
	@FindBy(xpath = "//*[text()='Intensive Outpatient']//following::input[1]")
	private WebElement IntensiveOutpatient;
	
	
	@FindBy(xpath = "//*[text()='Intensive Outpatient']//following::input[4]")
	private WebElement ASAM4;
	
	
		@FindBy(xpath = "//*[text()='Intensive Outpatient']//following::input[5]")
	private WebElement ASAM37;
	
	
		@FindBy(xpath = "//*[text()='Intensive Outpatient']//following::input[6]")
	private WebElement subASAM37;
	
		@FindBy(xpath = "//*[text()='Intensive Outpatient']//following::input[7]")
		private WebElement ASAM35;
	
		@FindBy(xpath = "//*[text()='Intensive Outpatient']//following::input[8]")
	private WebElement ASAM25;
	
		@FindBy(xpath = "//*[text()='Intensive Outpatient']//following::input[9]")
	private WebElement ASAM21;

		@FindBy(xpath = "//*[text()='Intensive Outpatient']//following::input[10]")
	private WebElement ASAM1;
	
	public void doentercaredetails() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Wait.waitUnitWebElementVisible(driver, Locations_lnk);
		ReusableMethods.javascriptclick(Locations_lnk, driver);
		Wait.untilPageLoadComplete(driver);
		Wait.waitUnitWebElementVisible(driver, individualtherapy);
		individualtherapy.sendKeys("Test");
		familytherapy.sendKeys("Test");
		staffratio.sendKeys("Test");
		dischargeplanning.sendKeys("Test");
		planningprocedures.sendKeys("Test");
		followupcare.sendKeys("Test");
		psychtesting.sendKeys("Test");
		PartialHospital.sendKeys("No");
		PartialHospital.sendKeys(Keys.ENTER);
		percentageofpatients.sendKeys("No");
		percentageofpatients.sendKeys(Keys.ENTER);
		
		int eles = ALOS.size();
		System.out.println(eles);
		for (int i = 0; i < eles; i++) {
			WebElement ele = ALOS.get(i);
			System.out.println(ele);
			ele.sendKeys("1");
		

	}
		
		
		externalorganizations.sendKeys("No");

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		externalorganizations.sendKeys(Keys.ENTER);
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		externalorganizations.sendKeys("No");
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ReusableMethods.javascriptclick(externalorganizations, driver);
		//ReusableMethods.clickdropdown(1);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ReusableMethods.javascriptclick(BehavioralMedicine, driver);

		ReusableMethods.uploadFileForStaffing("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, Done_btn1);
		ReusableMethods.javascriptclick(Done_btn1, driver);

		ReusableMethods.javascriptclick(ER, driver);

		ReusableMethods.uploadFileForStaffing("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, Done_btn1);
		ReusableMethods.javascriptclick(Done_btn1, driver);

		ReusableMethods.javascriptclick(HoldsRestraints, driver);

		ReusableMethods.uploadFileForStaffing("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, Done_btn1);
		ReusableMethods.javascriptclick(Done_btn1, driver);

		ReusableMethods.javascriptclick(DischargePlanning, driver);

		ReusableMethods.uploadFileForStaffing("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, Done_btn1);
		ReusableMethods.javascriptclick(Done_btn1, driver);
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//ReusableMethods.upload=1;
		
		ReusableMethods.javascriptclick(SaveCountinue_btn, driver);
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
