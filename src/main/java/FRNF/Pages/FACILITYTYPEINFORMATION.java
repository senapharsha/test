package FRNF.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import FNRFutil.ConfigFileReader;
import FNRFutil.ReusableMethods;
import FNRFutil.Wait;

public class FACILITYTYPEINFORMATION {
	WebDriver driver;
	ConfigFileReader configFileReader = null;
	ReusableMethods ReusableMethods = null;

	public FACILITYTYPEINFORMATION(WebDriver driver) {
		this.driver = driver;
		configFileReader = new ConfigFileReader();
		ReusableMethods = new ReusableMethods();
		PageFactory.initElements(driver, this);
	}
	
	
	
	@FindBy(xpath = "//*[text()='FACILITY TYPE INFORMATION']//following::label[1]")
	private WebElement LegalStatus_SEL;
	
	@FindBy(xpath = "//button[text()='Save & Continue']")
	private WebElement SaveCountinue_btn;
	
	public void enterfacilityinformation() {
		Wait.waitUnitWebElementVisible(driver, LegalStatus_SEL);
		Wait.waitUnitWebElementClickable(driver, LegalStatus_SEL);
		ReusableMethods.javascriptclick(LegalStatus_SEL, driver);
		ReusableMethods.javascriptclick(SaveCountinue_btn, driver);
	}
}
