package FRNF.Pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import FNRFutil.ConfigFileReader;
import FNRFutil.ReusableMethods;
import FNRFutil.Wait;

public class GeneralProfessionalLiability {
	WebDriver driver;
	ConfigFileReader configFileReader = null;
	ReusableMethods ReusableMethods = null;

	public GeneralProfessionalLiability(WebDriver driver) {
		this.driver = driver;
		configFileReader = new ConfigFileReader();
		ReusableMethods = new ReusableMethods();
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//*[text()='Are you Self Insured ?']//following::button[1]")
	private WebElement SelfInsured_SEL;
	
	@FindBy(xpath = "//*[text()='Professional and General liability insurance certificates showing limits, policy number(s) and expiration date(s).']//following::label[1]")
	private WebElement expiration_Browse;
	
	@FindBy(xpath = "/html/body/div[7]/div/div[2]/div/div[3]/div/span[2]/div/button")
	private WebElement Done_btn1;
	
	@FindBy(xpath = "//button[text()='Save & Continue']")
	private WebElement SaveCountinue_btn;
	
	public void doenterdetails() {
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Wait.untilPageLoadComplete(driver);
		Wait.waitUnitWebElementVisible(driver, SelfInsured_SEL);
		
		SelfInsured_SEL.sendKeys("Yes");
		SelfInsured_SEL.sendKeys(Keys.ENTER);
		ReusableMethods.javascriptclick(expiration_Browse, driver);
		ReusableMethods.uploadFile("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, Done_btn1);
		ReusableMethods.javascriptclick(Done_btn1, driver);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ReusableMethods.javascriptclick(SaveCountinue_btn, driver);
	}
	
}
