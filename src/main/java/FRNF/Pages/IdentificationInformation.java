package FRNF.Pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Map;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import FNRFutil.ConfigFileReader;
import FNRFutil.ReusableMethods;
import FNRFutil.Wait;

public class IdentificationInformation {
	WebDriver driver;
	ConfigFileReader configFileReader = null;
	ReusableMethods ReusableMethods = null;

	public IdentificationInformation(WebDriver driver) {
		this.driver = driver;
		configFileReader = new ConfigFileReader();
		ReusableMethods = new ReusableMethods();
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//*[text()='Facility Legal Name']//following::input[1]")
	private WebElement Facility_TXT;
	
	@FindBy(xpath = "//*[text()='Parent Company/Health System Name']//following::input[1]")
	private WebElement ParentCompany_TXT;

	@FindBy(xpath = "//*[text()='DBA (Identifying) Name']//following::input[1]")
	private WebElement DBA_TXT;
	
	@FindBy(xpath = "//*[text()='Website']//following::input[1]")
	private WebElement Website_TXT;
	
	@FindBy(xpath = "//*[text()='Credentialing Contact Email']//following::input[1]")
	private WebElement Credentialing_TXT;
	
	@FindBy(xpath = "//*[text()='Tax Identification Number']//following::input[1]")
	private WebElement TIN_TXT;
	
	@FindBy(xpath = "//*[text()='Administrative Address']//preceding::button[1]")
	private WebElement PracticState_TXT;
	
	
	@FindBy(xpath = "//*[text()='Administrative Address']//following::input[1]")
	private WebElement AdministrativeAddress_TXT;
	
	@FindBy(xpath = "//*[text()='Administrative City']//following::input[1]")
	private WebElement AdministrativeCity_TXT;
	
	
	@FindBy(xpath = "//*[text()='Administrative State']//following::button[1]")
	private WebElement AdministrativeState_TXT;
	
	@FindBy(xpath = "//*[text()='Administrative Zip']//following::input[1]")
	private WebElement AdministrativeZip_TXT;
	
	@FindBy(xpath = "//*[text()='Administrative County']//following::input[1]")
	private WebElement AdministrativeCounty_TXT;
	
	@FindBy(xpath = "//*[text()='Administrative Phone']//following::input[1]")
	private WebElement AdministrativePhone_TXT;
	
	@FindBy(xpath = "//*[text()='Administrative Fax']//following::input[1]")
	private WebElement AdministrativeFax_TXT;
	
	@FindBy(xpath = "//*[text()='Administrative Email']//following::input[1]")
	private WebElement AdministrativeEmail_TXT;
	
	@FindBy(xpath = "//*[text()='National Provider Identifier (NPI) Primary']//following::input[1]")
	private WebElement NPI_PRI_TXT;
	
	@FindBy(xpath = "//*[text()='National Provider Identifier (NPI) Secondary']//following::input[1]")
	private WebElement NPI_sec_TXT;
	
	@FindBy(xpath = "//*[text()='National Provider Identifier (NPI) Secondary']//following::button[1]")
	private WebElement healthSys_TXT;
	
	@FindBy(xpath = "//*[text()='Billing Address Same As Administrative Address']//preceding::span[1]")
	private WebElement NPI_check_TXT;
	
	@FindBy(xpath = "//*[text()='AK']//following::input[1]")
	private WebElement State_opt;
	
	
	@FindBy(xpath = "//*[text()='W9 form: If multiple tax ID numbers used, one W9 must be submitted for each.']//following::label")
	private WebElement Browse_btn;
	
	@FindBy(xpath = "/html/body/div[7]/div/div[2]/div/div[3]/div/span[2]/div/button")
	private WebElement Done_btn;
	
	
	
	@FindBy(xpath = "//*[@class='slds-button slds-button_icon slds-button_icon-brand']")
	private WebElement Check_btn;
	
	@FindBy(xpath = "//button[text()='Save & Continue']")
	private WebElement SaveCountinue_btn;
	
	public void fillIdentificationInformation(Map<String, String> xl) {
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Wait.untilPageLoadComplete(driver);
		Wait.waitUnitWebElementVisible(driver, TIN_TXT);
		//ReusableMethods.highlightenter(Facility_TXT,driver,xl.get("FacilityLegalName"),"Facility name");
		ReusableMethods.highlightenter(ParentCompany_TXT,driver,xl.get("ParentCompanyHealthSystemName"),"Parent Facility name");
		ReusableMethods.highlightenter(DBA_TXT,driver,xl.get("DBAName"),"DBA name");
		
		ReusableMethods.highlightenter(Website_TXT,driver,xl.get("Website"),"Website");
		//ReusableMethods.highlightenter(Credentialing_TXT,driver,xl.get("CredentialingContactEmail"),"Credentialing");
		ReusableMethods.highlightenter(TIN_TXT,driver,xl.get("TaxIdentificationNumber"),"TIN");
		
		
		
		//ReusableMethods.javascriptclick(PracticState_TXT, driver);
		//Wait.waitUnitWebElementVisible(driver, State_opt);
		ReusableMethods.selectState(PracticState_TXT,driver,xl.get("AdministrativeState"),"PracticState");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		PracticState_TXT.sendKeys(Keys.ENTER);
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ReusableMethods.highlightenter(AdministrativeAddress_TXT,driver,xl.get("AdministrativeAddress"),"AdministrativeAddress");
		ReusableMethods.highlightenter(AdministrativeCity_TXT,driver,xl.get("AdministrativeCity"),"AdministrativeCity");
		ReusableMethods.selectState(AdministrativeState_TXT,driver,xl.get("AdministrativeState"),"AdministrativeState");
		
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		AdministrativeState_TXT.sendKeys(Keys.ENTER);
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ReusableMethods.highlightenter(AdministrativeZip_TXT,driver,xl.get("AdministrativeZip"),"AdministrativeZip");
		ReusableMethods.highlightenter(AdministrativeCounty_TXT,driver,xl.get("AdministrativeCounty"),"AdministrativeCounty");
		
		ReusableMethods.highlightenter(AdministrativePhone_TXT,driver,xl.get("AdministrativePhone"),"AdministrativePhone");
		ReusableMethods.highlightenter(AdministrativeFax_TXT,driver,xl.get("AdministrativeFax"),"AdministrativeFax");
		ReusableMethods.highlightenter(AdministrativeEmail_TXT,driver,xl.get("AdministrativeEmail"),"AdministrativeEmail_TXT");
		
		NPI_PRI_TXT.sendKeys("1234567890");
		NPI_sec_TXT.sendKeys("2134567890");
		
		
		healthSys_TXT.sendKeys("No");
		ReusableMethods.sleep(1000);
		healthSys_TXT.sendKeys(Keys.ENTER);
		
		ReusableMethods.javascriptclick(NPI_check_TXT, driver);
		
		ReusableMethods.javascriptclick(Browse_btn, driver);
		
		ReusableMethods.uploadFile("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, Done_btn);
		ReusableMethods.javascriptclick(Done_btn, driver);
		
		Wait.waitUnitWebElementVisible(driver, Check_btn);
		
		ReusableMethods.javascriptclick(SaveCountinue_btn, driver);
		
	}
}
