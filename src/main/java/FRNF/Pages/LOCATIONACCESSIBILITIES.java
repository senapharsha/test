package FRNF.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.sikuli.script.Key;

import FNRFutil.ConfigFileReader;
import FNRFutil.ReusableMethods;
import FNRFutil.Wait;

public class LOCATIONACCESSIBILITIES {
	WebDriver driver;
	ConfigFileReader configFileReader = null;
	ReusableMethods ReusableMethods = null;

	public LOCATIONACCESSIBILITIES(WebDriver driver) {
		this.driver = driver;
		configFileReader = new ConfigFileReader();
		ReusableMethods = new ReusableMethods();
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath = "//*[text()='Public Transportation Access']//following::button[1]")
	private WebElement Accessibility_SEL;
	
	@FindBy(xpath = "//button[text()='Save & Continue']")
	private WebElement SaveCountinue_btn;
	
	public void addlocationaccessbility() {
		
		Wait.waitUnitWebElementVisible(driver, Accessibility_SEL);
		ReusableMethods.javascriptclick(Accessibility_SEL, driver);
		ReusableMethods.clickdropdown(0);
		ReusableMethods.javascriptclick(SaveCountinue_btn, driver);
	}
}
