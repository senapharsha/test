package FRNF.Pages;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import FNRFutil.ConfigFileReader;
import FNRFutil.ReusableMethods;
import FNRFutil.Wait;

public class LandingPage {
	WebDriver driver;
	ConfigFileReader configFileReader = null;
	ReusableMethods ReusableMethods = null;
	public static String cred="a0b2D000003s9ys";
	public static String state="MN";
	
	public static String MNcred="a0b2D000003rrZO";
	public static String MNstate="MN";
	
	public static String TNcred="a0b2D000003rQlX";  //a0b2D000003rzV3
	public static String TNstate="TN";
	
	public static String WIcred="a0b2D000003i57R";  //a0b2D000003iCza	a0b2D000003rzUd
	public static String WIstate="WI"; //Medica
	
	public static String WI_cred="a0b2D000003i57W";   //a0b2D000003iCzk	    a0b2D000003rzUJ
	public static String WI_state="WI"; //Medicaid
	
	public static String MOcred="a0b2D000003gqan";  //a0b2D000003iCNv		a0b2D000003rzxj
	public static String MOstate="MO";   
	
	public static String MAcred="a0b2D000003ibYs";  //a0b2D000003idCG    a0b2D000003idHp
	public static String MAstate="MA";
	
	public static String WAcred="a0b2D000003iCJA";   //a0b2D000003ryeh  //a0b2D000003rzLo	a0b2D000003s2BQ
	public static String WAstate="WA";
	
	public static String NEcred="a0b2D000003fQlH";		//a0b2D000003rzp4
	public static String NEstate="NE";  
	
	public static String KYcred="a0b2D000003gqpx";  // a0b2D000003h4JJ	a0b2D000003rzzL
	public static String KYstate="KY";  
	
	public static String VAcred="a0b2D000003fUHF";  //  a0b2D000003fON8		a0b2D000003rzzk
	public static String VAstate="VA";  
	
	public static String KScred="a0b2D000003fNxH";  // a0b2D000003iCPe		a0b2D000003iCPt		a0b2D000003rzz6
	public static String KSstate="KS"; 
	
	public static String NYcred="a0b2D000003g5mq";   // a0b2D000003iC3v
	public static String NYstate="NY"; 
	
	public static String UTcred="a0b2D000003hxrI";  //a0b2D000003ryeX
	public static String UTstate="UT";
	
	public static String toolehecred="a0b2D000003iAPA";
	public static String toolehestate="UT";
	
	public static String UTbothcred="a0b2D000003iB3U";  // a0b2D000003iCfM
	public static String UTbothstate="UT";

	public LandingPage(WebDriver driver) {
		this.driver = driver;
		configFileReader = new ConfigFileReader();
		ReusableMethods = new ReusableMethods();
		PageFactory.initElements(driver, this);
	}

	
	
	@FindBy(xpath = "//*[text()='Tax Identification Number']//following::input[1]")
	private WebElement TIN_TXT;

	@FindBy(xpath = "//button[text()='Search']")
	private WebElement Search_btn;
	
	@FindBy(xpath = "//button[text()='Start New Application']")
	private WebElement Startnew_btn;
	
    @FindBy(xpath = "//input[@id='username']")
    WebElement txt_UserName;

    @FindBy(xpath="//input[@id='password']")
    WebElement txt_Password;

    @FindBy(xpath="//input[@id='Login']")
    WebElement btn_Login;

    @FindBy(xpath = "//button[contains(@class, 'slds-button')]")
    WebElement btn_Finish;

	
	public void naviageURL() {
		driver.get(configFileReader.getApplicationUrl());
		
		Wait.untilPageLoadComplete(driver);
	}
	
	public void naviageSFURL() {
		driver.get("https://test.salesforce.com/");
		
		Wait.untilPageLoadComplete(driver);
	}
	
	public void naviageCredURL(String recID) {
		driver.get("https://providerforce--qaenv.my.salesforce.com/"+recID);
		
		Wait.untilPageLoadComplete(driver);
	}
	
	public void logIn() {
		txt_UserName.sendKeys("anil_k_vykuntham@optum.com.qaenv");
		txt_Password.sendKeys("optum#81");
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		btn_Login.click();
		
		Wait.waitUnitWebElementClickable(driver, btn_Finish);
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ReusableMethods.javascriptclick(btn_Finish, driver);
		
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void dosearch(Map<String, String> xl) {
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Wait.waitUnitWebElementVisible(driver, TIN_TXT);
		ReusableMethods.highlightenter(TIN_TXT,driver,xl.get("TaxIdentificationNumber"),"Txt");
		ReusableMethods.javascriptclick(Search_btn, driver);
		//Search_btn.click();
		Wait.waitUnitWebElementVisible(driver, Startnew_btn);
		//Startnew_btn.click();
		ReusableMethods.javascriptclick(Startnew_btn, driver);
	}
}
