package FRNF.Pages;

import java.util.Map;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import FNRFutil.ConfigFileReader;
import FNRFutil.ReusableMethods;
import FNRFutil.Wait;

public class LevelsofCare {
	WebDriver driver;
	ConfigFileReader configFileReader = null;
	ReusableMethods ReusableMethods = null;
	PaymentAppendixFacility PAF=null;

	public LevelsofCare(WebDriver driver) {
		this.driver = driver;
		configFileReader = new ConfigFileReader();
		ReusableMethods = new ReusableMethods();
		PAF=new PaymentAppendixFacility(driver);
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath = "//*[text()='Levels of Care the facility offers']//following::button[1]")
	public static WebElement LevelSof_SEL;

	@FindBy(xpath = "//*[text()='Both']")
	public static WebElement Both_btn;

	
	
	@FindBy(xpath = "//td[text()='I/P Locked']/following-sibling::td[1]")
	public static WebElement IPLockedAdult_CHK;
	
	@FindBy(xpath = "//td[text()='I/P Locked']/following-sibling::td[2]")
	public static WebElement IPLockedGeriatric_CHK;
	
	@FindBy(xpath = "//td[text()='I/P Locked']/following-sibling::td[3]")
	public static WebElement IPLockedAdolescent_CHK;
	
	@FindBy(xpath = "//td[text()='I/P Locked']/following-sibling::td[4]")
	public static WebElement IPLockedChild_CHK;
	
	@FindBy(xpath = "//td[text()='I/P Locked']/following-sibling::td[5]")
	public static WebElement IPLockedAll_CHK;
	
	
	@FindBy(xpath = "//td[text()='I/P Open']/following-sibling::td[1]")
	public static WebElement IPOpenAdult_CHK;
	
	@FindBy(xpath = "//td[text()='I/P Open']/following-sibling::td[2]")
	public static WebElement IPOpenGeriatric_CHK;
	
	@FindBy(xpath = "//td[text()='I/P Open']/following-sibling::td[3]")
	public static WebElement IPOpenAdolescent_CHK;
	
	@FindBy(xpath = "//td[text()='I/P Open']/following-sibling::td[4]")
	public static WebElement IPOpenChild_CHK;
	
	@FindBy(xpath = "//td[text()='I/P Open']/following-sibling::td[5]")
	public static WebElement IPOpenAll_CHK;
	
	
	@FindBy(xpath = "//td[text()='Residential']/following-sibling::td[1]")
	public static WebElement ResidentialAdult_CHK;
	
	@FindBy(xpath = "//td[text()='Residential']/following-sibling::td[2]")
	public static WebElement ResidentialGeriatric_CHK;
	
	@FindBy(xpath = "//td[text()='Residential']/following-sibling::td[3]")
	public static WebElement ResidentialAdolescent_CHK;
	
	@FindBy(xpath = "//td[text()='Residential']/following-sibling::td[4]")
	public static WebElement ResidentialChild_CHK;
	
	@FindBy(xpath = "//td[text()='Residential']/following-sibling::td[5]")
	public static WebElement ResidentialAll_CHK;
	
	@FindBy(xpath = "//td[text()='Partial Hospitalization (PHP)']/following-sibling::td[1]")
	public static WebElement PHPAdult_CHK;
	
	@FindBy(xpath = "//td[text()='Partial Hospitalization (PHP)']/following-sibling::td[2]")
	public static WebElement PHPGeriatric_CHK;
	
	@FindBy(xpath = "//td[text()='Partial Hospitalization (PHP)']/following-sibling::td[3]")
	public static WebElement PHPAdolescent_CHK;
	
	@FindBy(xpath = "//td[text()='Partial Hospitalization (PHP)']/following-sibling::td[4]")
	public static WebElement PHPChild_CHK;
	
	@FindBy(xpath = "//td[text()='Partial Hospitalization (PHP)']/following-sibling::td[5]")
	public static WebElement PHPAll_CHK;
	
	@FindBy(xpath = "//td[text()='MH Intensive Outpatient (IOP)']/following-sibling::td[1]")
	public static WebElement IOPAdult_CHK;
	
	@FindBy(xpath = "//td[text()='MH Intensive Outpatient (IOP)']/following-sibling::td[2]")
	public static WebElement IOPGeriatric_CHK;
	
	@FindBy(xpath = "//td[text()='MH Intensive Outpatient (IOP)']/following-sibling::td[3]")
	public static WebElement IOPAdolescent_CHK;
	
	@FindBy(xpath = "//td[text()='MH Intensive Outpatient (IOP)']/following-sibling::td[4]")
	public static WebElement IOPChild_CHK;
	
	@FindBy(xpath = "//td[text()='MH Intensive Outpatient (IOP)']/following-sibling::td[5]")
	public static WebElement IOPAll_CHK;
	
	@FindBy(xpath = "//td[text()='Crisis Services (i.e. stabilization, 23 hr. obs)']/following-sibling::td[1]")
	public static WebElement CrisisServicesAdult_CHK;
	
	@FindBy(xpath = "//td[text()='Crisis Services (i.e. stabilization, 23 hr. obs)']/following-sibling::td[2]")
	public static WebElement CrisisServicesGeriatric_CHK;
	
	@FindBy(xpath = "//td[text()='Crisis Services (i.e. stabilization, 23 hr. obs)']/following-sibling::td[3]")
	public static WebElement CrisisServicesAdolescent_CHK;
	
	@FindBy(xpath = "//td[text()='Crisis Services (i.e. stabilization, 23 hr. obs)']/following-sibling::td[4]")
	public static WebElement CrisisServicesChild_CHK;
	
	@FindBy(xpath = "//td[text()='Crisis Services (i.e. stabilization, 23 hr. obs)']/following-sibling::td[5]")
	public static WebElement CrisisServicesAll_CHK;
	
	
	@FindBy(xpath = "//td[contains(text(), 'Medically Managed Intensive Inpatient Services ASAM 4')]/following-sibling::td[1]")
	public static WebElement ASAM4Adult_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Medically Managed Intensive Inpatient Services ASAM 4')]/following-sibling::td[2]")
	public static WebElement ASAM4Geriatric_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Medically Managed Intensive Inpatient Services ASAM 4')]/following-sibling::td[3]")
	public static WebElement ASAM4Adolescent_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Medically Managed Intensive Inpatient Services ASAM 4')]/following-sibling::td[4]")
	public static WebElement ASAM4All_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Medically Monitored intensive Inpatient Services ASAM 3.7 WM')]/following-sibling::td[1]")
	public static WebElement ASAM37WMAdult_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Medically Monitored intensive Inpatient Services ASAM 3.7 WM')]/following-sibling::td[2]")
	public static WebElement ASAM37WMGeriatric_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Medically Monitored intensive Inpatient Services ASAM 3.7 WM')]/following-sibling::td[3]")
	public static WebElement ASAM37WMAdolescent_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Medically Monitored intensive Inpatient Services ASAM 3.7 WM')]/following-sibling::td[4]")
	public static WebElement ASAM37WMAll_CHK;
	
	
	@FindBy(xpath = "//td[contains(text(), 'Medically Monitored Intensive Inpatient Services (SUD Residential) ASAM 3.7')]/following-sibling::td[1]")
	public static WebElement ASAM37Adult_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Medically Monitored Intensive Inpatient Services (SUD Residential) ASAM 3.7')]/following-sibling::td[2]")
	public static WebElement ASAM37Geriatric_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Medically Monitored Intensive Inpatient Services (SUD Residential) ASAM 3.7')]/following-sibling::td[3]")
	public static WebElement ASAM37Adolescent_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Medically Monitored Intensive Inpatient Services (SUD Residential) ASAM 3.7')]/following-sibling::td[4]")
	public static WebElement ASAM37All_CHK;
	
	
	
	@FindBy(xpath = "//td[contains(text(), 'Clinically Managed High-Intensity Residential Services ASAM 3.5')]/following-sibling::td[1]")
	public static WebElement ASAM35Adult_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Clinically Managed High-Intensity Residential Services ASAM 3.5')]/following-sibling::td[2]")
	public static WebElement ASAM35Geriatric_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Clinically Managed High-Intensity Residential Services ASAM 3.5')]/following-sibling::td[3]")
	public static WebElement ASAM35Adolescent_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Clinically Managed High-Intensity Residential Services ASAM 3.5')]/following-sibling::td[4]")
	public static WebElement ASAM35All_CHK;
	
	
	@FindBy(xpath = "//td[contains(text(), 'Partial Hospitalization (PHP) – ASAM 2.5')]/following-sibling::td[3]")
	public static WebElement ASAM25Adolescent_CHK;
	
	
	@FindBy(xpath = "//td[contains(text(), 'Partial Hospitalization (PHP) – ASAM 2.5')]/following-sibling::td[1]")
	public static WebElement ASAM25Adult_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Partial Hospitalization (PHP) – ASAM 2.5')]/following-sibling::td[2]")
	public static WebElement ASAM25Geriatric_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Partial Hospitalization (PHP) – ASAM 2.5')]/following-sibling::td[4]")
	public static WebElement ASAM25All_CHK;
	
	
	@FindBy(xpath = "//td[contains(text(), 'SUD Intensive Outpatient (IOP) – ASAM 2.1')]/following-sibling::td[3]")
	public static WebElement ASAM21Adolescent_CHK;
	
	
	@FindBy(xpath = "//td[contains(text(), 'SUD Intensive Outpatient (IOP) – ASAM 2.1')]/following-sibling::td[1]")
	public static WebElement ASAM21Adult_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'SUD Intensive Outpatient (IOP) – ASAM 2.1')]/following-sibling::td[2]")
	public static WebElement ASAM21Geriatric_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'SUD Intensive Outpatient (IOP) – ASAM 2.1')]/following-sibling::td[4]")
	public static WebElement ASAM21All_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Ambulatory Detox (Drug or Alcohol) – ASAM 1 WM')]/following-sibling::td[3]")
	public static WebElement ASAM1WMAdolescent_CHK;
	
	
	@FindBy(xpath = "//td[contains(text(), 'Ambulatory Detox (Drug or Alcohol) – ASAM 1 WM')]/following-sibling::td[1]")
	public static WebElement ASAM1WMAdult_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Ambulatory Detox (Drug or Alcohol) – ASAM 1 WM')]/following-sibling::td[2]")
	public static WebElement ASAM1WMGeriatric_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Ambulatory Detox (Drug or Alcohol) – ASAM 1 WM')]/following-sibling::td[4]")
	public static WebElement ASAM1WMAll_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Outpatient Clinic – ASAM 1')]/following-sibling::td[1]")
	public static WebElement ASAM1Adult_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Outpatient Clinic – ASAM 1')]/following-sibling::td[2]")
	public static WebElement ASAM1Geriatric_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Outpatient Clinic – ASAM 1')]/following-sibling::td[3]")
	public static WebElement ASAM1Adolescent_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Outpatient Clinic – ASAM 1')]/following-sibling::td[4]")
	public static WebElement ASAM1All_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Opioid Treatment Program')]/following-sibling::td[1]")
	public static WebElement OTPAdult_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Opioid Treatment Program')]/following-sibling::td[2]")
	public static WebElement OTPGeriatric_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Opioid Treatment Program')]/following-sibling::td[3]")
	public static WebElement OTPAdolescent_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Opioid Treatment Program')]/following-sibling::td[4]")
	public static WebElement OTPAll_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Other')]/following-sibling::td[1]")
	public static WebElement OtherAdult_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Other')]/following-sibling::td[2]")
	public static WebElement OtherGeriatric_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Other')]/following-sibling::td[3]")
	public static WebElement OtherAdolescent_CHK;
	
	@FindBy(xpath = "//td[contains(text(), 'Other')]/following-sibling::td[4]")
	public static WebElement OtherAll_CHK;
	
	
	@FindBy(xpath = "//*[text()='ECT']//following::input[1]")
	public static WebElement ECT1_CHK;
	
	@FindBy(xpath = "//*[text()='ECT']//following::input[2]")
	public static WebElement ECT2_CHK;
	
	@FindBy(xpath = "//*[text()='ECT']//following::input[3]")
	public static WebElement ECT3_CHK;
	
	@FindBy(xpath = "//*[text()='ECT']//following::input[4]")
	public static WebElement ECT4_CHK;
	
	@FindBy(xpath = "//*[text()='Both']")
	public static WebElement Both_btn1;
	
	@FindBy(xpath = "//*[text()='Both']")
	public static WebElement Both_btn2;
	
	@FindBy(xpath = "//*[text()='Both']")
	public static WebElement Both_btn3;
	
	@FindBy(xpath = "//*[text()='Both']")
	public static WebElement Both_btn4;
	

	
	
	@FindBy(xpath = "//button[text()='Save & Continue']")
	public static WebElement SaveCountinue_btn;
	
	public void setcontractorstatus(Map<String, String> xl) {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Wait.waitUnitWebElementVisible(driver, LevelSof_SEL);
		LevelSof_SEL.sendKeys("Both");
		LevelSof_SEL.sendKeys(Keys.ENTER);
		
		
		
		Wait.waitUnitWebElementVisible(driver, IPLockedAdult_CHK);
		
		ReusableMethods.CheckBtn_click(LevelsofCare.IPLockedAdult_CHK, driver, xl.get("IPLockedAdult"), "IPLockedAdult");
		ReusableMethods.CheckBtn_click(LevelsofCare.IPLockedGeriatric_CHK, driver, xl.get("IPLockedGeriatric"), "IPLockedGeriatric");
		ReusableMethods.CheckBtn_click(LevelsofCare.IPLockedAdolescent_CHK, driver, xl.get("IPLockedAdolescent"), "IPLockedAdolescent");
		ReusableMethods.CheckBtn_click(LevelsofCare.IPLockedChild_CHK, driver, xl.get("IPLockedChild"), "IPLockedChild");
		ReusableMethods.CheckBtn_click(LevelsofCare.IPLockedAll_CHK, driver, xl.get("IPLockedSelectAll"), "IPLockedSelectAll");
		
		ReusableMethods.CheckBtn_click(LevelsofCare.IPOpenAdolescent_CHK, driver, xl.get("IPOpenAdolescent"), "IPOpenAdolescent");
		ReusableMethods.CheckBtn_click(LevelsofCare.IPOpenGeriatric_CHK, driver, xl.get("IPOpenGeriatric"), "IPOpenGeriatric");
		ReusableMethods.CheckBtn_click(LevelsofCare.IPOpenAdolescent_CHK, driver, xl.get("IPOpenAdolescent"), "IPOpenAdolescent");
		ReusableMethods.CheckBtn_click(LevelsofCare.IPOpenGeriatric_CHK, driver, xl.get("IPOpenGeriatric"), "IPOpenGeriatric");
		ReusableMethods.CheckBtn_click(LevelsofCare.IPOpenAll_CHK, driver, xl.get("IPOpenSelectAll"), "IPOpenSelectAll");
		
		
		ReusableMethods.CheckBtn_click(LevelsofCare.ResidentialAdolescent_CHK, driver, xl.get("ResidentialAdolescent"), "ResidentialAdolescent");
		ReusableMethods.CheckBtn_click(LevelsofCare.ResidentialAdult_CHK, driver, xl.get("ResidentialAdult"), "ResidentialAdult");
		ReusableMethods.CheckBtn_click(LevelsofCare.ResidentialChild_CHK, driver, xl.get("ResidentialChild"), "ResidentialChild");
		ReusableMethods.CheckBtn_click(LevelsofCare.ResidentialGeriatric_CHK, driver, xl.get("ResidentialGeriatric"), "ResidentialGeriatric");
		ReusableMethods.CheckBtn_click(LevelsofCare.ResidentialAll_CHK, driver, xl.get("ResidentialSelectAll"), "ResidentialSelectAll");
		
		
		ReusableMethods.CheckBtn_click(LevelsofCare.PHPAdolescent_CHK, driver, xl.get("PHPAdolescent"), "PHPAdolescent");
		ReusableMethods.CheckBtn_click(LevelsofCare.PHPAdult_CHK, driver, xl.get("PHPAdult"), "PHPAdult");
		ReusableMethods.CheckBtn_click(LevelsofCare.PHPChild_CHK, driver, xl.get("PHPChild"), "PHPChild");
		ReusableMethods.CheckBtn_click(LevelsofCare.PHPGeriatric_CHK, driver, xl.get("PHPGeriatric"), "PHPGeriatric");
		ReusableMethods.CheckBtn_click(LevelsofCare.PHPAll_CHK, driver, xl.get("PHPSelectAll"), "PHPSelectAll");
		
		ReusableMethods.CheckBtn_click(LevelsofCare.IOPAdolescent_CHK, driver, xl.get("IOPAdolescent"), "IOPAdolescent");
		ReusableMethods.CheckBtn_click(LevelsofCare.IOPAdult_CHK, driver, xl.get("IOPAdult"), "IOPAdult");
		ReusableMethods.CheckBtn_click(LevelsofCare.IOPChild_CHK, driver, xl.get("IOPChild"), "IOPChild");
		ReusableMethods.CheckBtn_click(LevelsofCare.IOPGeriatric_CHK, driver, xl.get("IOPGeriatric"), "IOPGeriatric");
		ReusableMethods.CheckBtn_click(LevelsofCare.IOPAll_CHK, driver, xl.get("IOPSelectAll"), "IOPSelectAll");
		
		ReusableMethods.CheckBtn_click(LevelsofCare.CrisisServicesAdolescent_CHK, driver, xl.get("CrisisServicesAdolescent"), "CrisisServicesAdolescent");
		ReusableMethods.CheckBtn_click(LevelsofCare.CrisisServicesAdult_CHK, driver, xl.get("CrisisServicesAdult"), "CrisisServicesAdult");
		ReusableMethods.CheckBtn_click(LevelsofCare.CrisisServicesChild_CHK, driver, xl.get("CrisisServicesChild"), "CrisisServicesChild");
		ReusableMethods.CheckBtn_click(LevelsofCare.CrisisServicesGeriatric_CHK, driver, xl.get("CrisisServicesGeriatric"), "CrisisServicesGeriatric");
		ReusableMethods.CheckBtn_click(LevelsofCare.CrisisServicesAll_CHK, driver, xl.get("CrisisServicesSelectAll"), "CrisisServicesSelectAll");
		/*
		try {
		ECT1_CHK.sendKeys("Both");
		Thread.sleep(1500);
		ECT1_CHK.sendKeys(Keys.ENTER);
		ECT2_CHK.sendKeys("Both");
		Thread.sleep(1000);
		ECT2_CHK.sendKeys(Keys.ENTER);
		ECT3_CHK.sendKeys("Both");
		Thread.sleep(1000);
		ECT3_CHK.sendKeys(Keys.ENTER);
		ECT4_CHK.sendKeys("Both");
		Thread.sleep(1000);
		ECT4_CHK.sendKeys(Keys.ENTER);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM4Adolescent_CHK, driver, xl.get("ASAM4Adolescent"), "ASAM4Adolescent");
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM4Adult_CHK, driver, xl.get("ASAM4Adult"), "ASAM4Adult");
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM4Geriatric_CHK, driver, xl.get("ASAM4Geriatric"), "ASAM4Geriatric");
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM4All_CHK, driver, xl.get("ASAM4SelectAll"), "ASAM4SelectAll");
		
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM37WMAdolescent_CHK, driver, xl.get("ASAM37WMAdolescent"), "ASAM37WMAdolescent");
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM37WMAdult_CHK, driver, xl.get("ASAM37WMAdult"), "ASAM37WMAdult");
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM37WMGeriatric_CHK, driver, xl.get("ASAM37WMGeriatric"), "ASAM37WMGeriatric");
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM37WMAll_CHK, driver, xl.get("ASAM37WMSelectAll"), "ASAM37WMSelectAll");
		
		PAF.scrollDownVertically();
		ReusableMethods.sleep(3000);
		
		
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM37Adolescent_CHK, driver, xl.get("ASAM37Adolescent"), "ASAM37Adolescent");
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM37Adult_CHK, driver, xl.get("ASAM37Adult"), "ASAM37Adult");
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM37Geriatric_CHK, driver, xl.get("ASAM37Geriatric"), "ASAM37Geriatric");
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM37All_CHK, driver, xl.get("ASAM3.7SelectAll"), "ASAM3.7SelectAll");
		//ReusableMethods.javascriptclick(ASAM37All_CHK, driver);
		
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM35Adult_CHK, driver, xl.get("ASAM3.5Adult"), "ASAM3.5Adult");
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM35Geriatric_CHK, driver, xl.get("ASAM3.5Geriatric"), "ASAM3.5Geriatric");
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM35Adolescent_CHK, driver, xl.get("ASAM3.5Adolescent"), "ASAM3.5Adolescent");
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM35All_CHK, driver, xl.get("ASAM3.5SelectAll"), "ASAM3.5SelectAll");
//		ReusableMethods.javascriptclick(ASAM35All_CHK, driver);
		
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM25Adult_CHK, driver, xl.get("ASAM2.5Adult"), "ASAM2.5Adult");
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM25Geriatric_CHK, driver, xl.get("ASAM2.5Geriatric"), "ASAM2.5Geriatric");
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM25Adolescent_CHK, driver, xl.get("ASAM2.5Adolescent"), "ASAM2.5Adolescent");
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM25All_CHK, driver, xl.get("ASAM2.5SelectAll"), "ASAM2.5SelectAll");
//		ReusableMethods.javascriptclick(ASAM25All_CHK, driver);
		
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM21Adult_CHK, driver, xl.get("ASAM2.1Adult"), "ASAM2.1Adult");
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM21Geriatric_CHK, driver, xl.get("ASAM2.1Geriatric"), "ASAM2.1Geriatric");
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM21Adolescent_CHK, driver, xl.get("ASAM2.1Adolescent"), "ASAM2.1Adolescent");
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM21All_CHK, driver, xl.get("ASAM2.1SelectAll"), "ASAM2.1SelectAll");
		
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM1WMAdult_CHK, driver, xl.get("ASAM1WMAdult"), "ASAM1WMAdult");
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM1WMGeriatric_CHK, driver, xl.get("ASAM1WMGeriatric"), "ASAM1WMGeriatric");
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM1WMAdolescent_CHK, driver, xl.get("ASAM1WMAdolescent"), "ASAM1WMAdolescent");
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM1WMAll_CHK, driver, xl.get("ASAM1WMSelectAll"), "ASAM1WMSelectAll");
//		ReusableMethods.javascriptclick(ASAM1WMAll_CHK, driver);
		
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM1Adult_CHK, driver, xl.get("ASAM1Adult"), "ASAM1Adult");
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM1Geriatric_CHK, driver, xl.get("ASAM1Geriatric"), "ASAM1Geriatric");
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM1Adolescent_CHK, driver, xl.get("ASAM1Adolescent"), "ASAM1Adolescent");
		ReusableMethods.CheckBtn_click(LevelsofCare.ASAM1All_CHK, driver, xl.get("ASAM1SelectAll"), "ASAM1SelectAll");
//		ReusableMethods.javascriptclick(ASAM1WMAll_CHK, driver);
		
		ReusableMethods.CheckBtn_click(LevelsofCare.OTPAdult_CHK, driver, xl.get("OpioidAdult"), "OpioidAdult");
		ReusableMethods.CheckBtn_click(LevelsofCare.OTPGeriatric_CHK, driver, xl.get("OpioidGeriatric"), "OpioidGeriatric");
		ReusableMethods.CheckBtn_click(LevelsofCare.OTPAdolescent_CHK, driver, xl.get("OpioidAdolescent"), "OpioidAdolescent");
		ReusableMethods.CheckBtn_click(LevelsofCare.OTPAll_CHK, driver, xl.get("OpioidSelectAll"), "OpioidSelectAll");
//		ReusableMethods.javascriptclick(OTPAll_CHK, driver);
		
		ReusableMethods.CheckBtn_click(LevelsofCare.OtherAdult_CHK, driver, xl.get("OtherAdult"), "OtherAdult");
		ReusableMethods.CheckBtn_click(LevelsofCare.OtherGeriatric_CHK, driver, xl.get("OtherGeriatric"), "OtherGeriatric");
		ReusableMethods.CheckBtn_click(LevelsofCare.OtherAdolescent_CHK, driver, xl.get("OtherAdolescent"), "OtherAdolescent");
		ReusableMethods.CheckBtn_click(LevelsofCare.OtherAll_CHK, driver, xl.get("OtherSelectAll"), "OtherSelectAll");
//		ReusableMethods.javascriptclick(OtherAll_CHK, driver);
		
		ReusableMethods.javascriptclick(SaveCountinue_btn, driver);
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
	
	
}
