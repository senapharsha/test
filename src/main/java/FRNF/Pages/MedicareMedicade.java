package FRNF.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import FNRFutil.ConfigFileReader;
import FNRFutil.ReusableMethods;
import FNRFutil.Wait;

public class MedicareMedicade {

	WebDriver driver;
	ConfigFileReader configFileReader = null;
	ReusableMethods ReusableMethods = null;

	public MedicareMedicade(WebDriver driver) {
		this.driver = driver;
		configFileReader = new ConfigFileReader();
		ReusableMethods = new ReusableMethods();
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//*[text()='Commercial']//following::label")
	private WebElement Commercial;

	@FindBy(xpath = "//*[text()='Medicare ']//following::label")
	private WebElement Medicare;

	@FindBy(xpath = "//*[text()='Medicaid ']//following::label")
	private WebElement Medicaid;

	@FindBy(xpath ="//*[text()='Behavioral Health Medicare ID Number']//following::input[1]")
	private WebElement MedicareID;

	@FindBy(xpath ="//*[text()='Behavioral Health Medicaid ID Number']//following::input[1]")
	private WebElement MedicaidID;

	@FindBy(xpath = "//button[text()='Save & Continue']")
	private WebElement SaveCountinue_btn;
	
	@FindBy(xpath = "//*[text()='Medicare Certification Letter with Medicare Number']//following::label")
	private WebElement MedicareBrowse_btn;
	
	@FindBy(xpath = "//*[text()='Medicaid Certification Letter with Medicaid Number']//following::label")
	private WebElement MedicaidBrowse_btn;
	
	@FindBy(xpath = "//*[text()='Copy of completed Ownership and Disclosure Form (REQUIRED if applying for participation in Medicaid networks)']//following::label")
	private WebElement DisclosureBrowse_btn;
	
	@FindBy(xpath = "/html/body/div[7]/div/div[2]/div/div[3]/div/span[2]/div/button")
	private WebElement Done_btn1;
	
	public void doentermedicaremedicade() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Wait.waitUnitWebElementVisible(driver, Commercial);
		ReusableMethods.javascriptclick(Commercial, driver);
		ReusableMethods.javascriptclick(Medicare, driver);
		ReusableMethods.javascriptclick(Medicaid, driver);
		Wait.waitUnitWebElementClickable(driver, MedicareID);
		MedicareID.sendKeys("MCRE001");
		MedicaidID.sendKeys("MCID001");
		
		
		ReusableMethods.javascriptclick(MedicareBrowse_btn, driver);
		
		ReusableMethods.uploadFile("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, Done_btn1);
		ReusableMethods.javascriptclick(Done_btn1, driver);
		
		ReusableMethods.javascriptclick(MedicaidBrowse_btn, driver);
		
		ReusableMethods.uploadFile("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, Done_btn1);
		ReusableMethods.javascriptclick(Done_btn1, driver);
		
		ReusableMethods.javascriptclick(DisclosureBrowse_btn, driver);
		
		ReusableMethods.uploadFile("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, Done_btn1);
		ReusableMethods.javascriptclick(Done_btn1, driver);
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ReusableMethods.javascriptclick(SaveCountinue_btn, driver);
	}
}
