package FRNF.Pages;

import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import FNRFutil.Wait;
import FNRFutil.ConfigFileReader;
import FNRFutil.ReusableMethods;

public class PaymentAppendixFacility {
	
	WebDriver driver;
	ConfigFileReader configFileReader = null;
	ReusableMethods ReusableMethods = null;
	
	public static String fcWindow;

	public PaymentAppendixFacility(WebDriver driver) {
		this.driver = driver;
		configFileReader = new ConfigFileReader();
		ReusableMethods = new ReusableMethods();
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(xpath = "//a[contains(text(),'FC-')]")
	public static WebElement FacilityContract_LNK;
	
	@FindBy(xpath = "//td[@id='topButtonRow']//input[@title='Payment Appendix']")
	public static WebElement PaymentAppendix_BTN;
	
	@FindBy(xpath = "//button[@title='Fresh Rates']")
	public static WebElement freshRates_BTN;
	
		 @FindBy(xpath = "//input[@name='Medicaid']")
		 public static List<WebElement> mcaid_TXT;
		 
		 @FindBy(xpath = "//input[@name='Medicare']") 
		 public static List<WebElement> mcare_TXT;
		 
		 @FindBy(xpath = "//input[@name='Commercial']") 
		 public static List<WebElement> comm_TXT;
		 
		 @FindBy(xpath = "//input[@class='slds-input']") 
		 public static List<WebElement> inputFields_TXT;
		 
		 @FindBy(xpath = "//select[@name='OptumSigner']")
			public static WebElement OptumSigner_select;
		 
		 @FindBy(xpath = "//input[@name='SiteLocation']") 
		 public static List<WebElement> SiteLocation_TXT;
		 
		 @FindBy(xpath = "//input[@name='Modifier']") 
		 public static List<WebElement> Modifier_TXT;
		 
		 @FindBy(xpath = "//input[@name='Unit']") 
		 public static List<WebElement> Unit_TXT;
		 
		 @FindBy(xpath = "//input[@name='Rate']") 
		 public static List<WebElement> Rate_TXT;
		 
		 @FindBy(xpath = "//input[@name='Site Location']") 
		 public static List<WebElement> Non_stand_SiteLocation_TXT;
		
		 @FindBy(xpath = "//div[@title='MO Medicaid']/following::input[@value='Create New FPA']") 
		 public static WebElement CreateMOMedicaid_BTN;
		 
		 @FindBy(xpath = "//div[@title='KS KANCARE']/following::input[@value='Create New FPA']") 
		 public static WebElement CreateKSMedicaid_BTN;
		 
		 @FindBy(xpath = "//div[@title='WA Medicaid']/following::input[@value='Create New FPA']") 
		 public static WebElement CreateWAMedicaid_BTN;
		 
		 @FindBy(xpath = "//div[@title='VA Medicaid']/following::input[@value='Create New FPA']") 
		 public static WebElement CreateVAMedicaid_BTN;
		 
		 @FindBy(xpath = "//div[@title='1005 TennCare']/following::input[@value='Create New FPA']") 
		 public static WebElement CreateTNMedicaid_BTN;
		 
		 @FindBy(xpath = "//div[@title='NE Medicaid']/following::input[@value='Create New FPA']") 
		 public static WebElement CreateNEMedicaid_BTN;
		 
		 @FindBy(xpath = "//div[@title='NY IPA']/following::input[@value='Create New FPA']") 
		 public static WebElement CreateNYMedicaid_BTN;
		 
		 @FindBy(xpath = "//div[@title='KY Medicaid']/following::input[@value='Create New FPA']") 
		 public static WebElement CreateKYMedicaid_BTN;
		 
		 @FindBy(xpath = "//div[@title='WI Medica']/following::input[@value='Create New FPA']") 
		 public static WebElement CreateWIMedica_BTN;
		 
		 @FindBy(xpath = "//div[@title='WI Medicaid']/following::input[@value='Create New FPA']") 
		 public static WebElement CreateWIMedicaid_BTN;
		 
		 @FindBy(xpath = "//div[@title='UT Medicaid']/following::input[@value='Create New FPA']") 
		 public static WebElement CreateUTMedicaid_BTN;
		 
		 @FindBy(xpath = "//div[contains(@title,'2008')]/following::input[@value='Create New FPA']") 
		 public static WebElement CreateStandardFPA_BTN;
		 
		 @FindBy(xpath = "//p[text()='Non Standard Payment Appendix']") 
		 public static WebElement header_text;
		 
		 @FindBy(xpath = "//button[@title='add']") 
		 public static WebElement add_BTN;
		 
		 @FindBy(xpath = "//input[@name='Site Location']/ancestor::tr/following::input[1]") 
		 public static List<WebElement> perDiem_VA_txt;
		
		 @FindBy(xpath = "//input[@name='Site Location']/preceding::input[2]") 
		 public static List<WebElement> NE_Modfier_TXT;
		 
		 @FindBy(xpath = "//input[@name='Site Location']/preceding::input[4]")
		 public static List<WebElement> MOrate_TXT;
		 
		 @FindBy(xpath = "//input[@name='Site Location']/preceding::input[2]")
		 public static List<WebElement> KSperDiem_TXT;
		 
		 @FindBy(xpath = "//input[@name='Modifier']/preceding::input[1]") 
		 public static List<WebElement> HCPC_TXT;
		 	
		 @FindBy(xpath = "//button[@title='delete']/preceding::input[1]") 
		 public static List<WebElement> SL_TXT;
		 
		 public void clickPaymentAppendix() {
			 
			 scrollDownVertically();
				FacilityContract_LNK.click();
				ReusableMethods.sleep(5000);
				
				fcWindow=driver.getWindowHandle();
				System.out.println("Main Window : "+driver.getTitle());
				
				PaymentAppendix_BTN.click();
				ReusableMethods.sleep(5000);
				
				String mulFPAWindow=ReusableMethods.switchWindow(driver, fcWindow);
				//Wait.waitUnitWebElementClickable(driver, CreateMOMedicaid_BTN);
				ReusableMethods.sleep(5000);
			 
		 }
		 
		 public void FillPaymentAppendix() {
			 CreateStandardFPA_BTN.click();
			 ReusableMethods.sleep(30000);
			 
			 
				System.out.println(inputFields_TXT.size());
				
				Iterator<WebElement> iter = inputFields_TXT.iterator();
		
				while(iter.hasNext()) {
				    WebElement input_ele = iter.next();
				
					if(input_ele.getAttribute("value").length() == 0) {
						
						String rate = Integer.toString(randomNum(100, 999));
						if(input_ele.isDisplayed() )	{		
							input_ele.sendKeys(rate);
							}
					}
					
				}
				
				
				Iterator<WebElement> iter1 = SiteLocation_TXT.iterator();
				
						while(iter1.hasNext()) {
						    WebElement input_ele = iter1.next();
						
							if(input_ele.getAttribute("value").length() == 0) {
								if(input_ele.isDisplayed())	{
									input_ele.clear();;
									input_ele.sendKeys("1");}
							}
							
						}
			 
			 
		 }
		 
		 
		 public void FillMOPaymentAppendix() {

		CreateMOMedicaid_BTN.click();
		
		ReusableMethods.sleep(2000);
		
		
		try {
		driver.switchTo().alert().accept();
		ReusableMethods.sleep(2000);
		driver.switchTo().defaultContent();
		ReusableMethods.sleep(10000);
		}
		catch (Exception Ex)
		{
			System.out.println("No Alert present.");
		}
		
		ReusableMethods.sleep(25000);
		
		//ReusableMethods.selectUsingValue(OptumSigner_select, "Anil Facility");
		
	//filling site-location
		
	Iterator<WebElement>  iter2 = Non_stand_SiteLocation_TXT.iterator();
		System.out.println(Non_stand_SiteLocation_TXT.size());
		int n=Non_stand_SiteLocation_TXT.size();
		while(iter2.hasNext()) {
		    WebElement input_ele = iter2.next();
		
			if(input_ele.getAttribute("value").length() == 0) {
				if(input_ele.isDisplayed())	{
					input_ele.clear();;
					input_ele.sendKeys("1");}
			}
			
		}
		
		//filling rates
		
		Iterator<WebElement>  iter3 = MOrate_TXT.iterator();
		System.out.println(MOrate_TXT.size());
		
		while(iter3.hasNext()) {
		    WebElement input_ele = iter3.next();
		
			if(input_ele.getAttribute("value").length() == 0) {
				if(input_ele.isDisplayed())	{
					input_ele.clear();
					input_ele.sendKeys(Integer.toString(randomNum(100, 999)));}
			}
			
		}
		
		
		 }
		 
		 
		 
		 public void FillTNPaymentAppendix() {

				CreateTNMedicaid_BTN.click();
				
				ReusableMethods.sleep(2000);
				
				
				try {
				driver.switchTo().alert().accept();
				ReusableMethods.sleep(2000);
				driver.switchTo().defaultContent();
				ReusableMethods.sleep(10000);
				}
				catch (Exception Ex)
				{
					System.out.println("No Alert present.");
				}
				
				ReusableMethods.sleep(25000);
				
				//ReusableMethods.selectUsingValue(OptumSigner_select, "Anil Facility");
				
			//filling site-location
				
			Iterator<WebElement>  iter2 = Unit_TXT.iterator();
				System.out.println(Unit_TXT.size());
				int n=Unit_TXT.size();
				while(iter2.hasNext()) {
				    WebElement input_ele = iter2.next();
				
					if(input_ele.getAttribute("value").length() == 0) {
						if(input_ele.isDisplayed())	{
							input_ele.clear();;
							input_ele.sendKeys("per diem");}
					}
					
				}
				
				//filling rates
				
				Iterator<WebElement>  iter3 = Rate_TXT.iterator();
				System.out.println(Rate_TXT.size());
				
				while(iter3.hasNext()) {
				    WebElement input_ele = iter3.next();
				
					if(input_ele.getAttribute("value").length() == 0) {
						if(input_ele.isDisplayed())	{
							input_ele.clear();
							input_ele.sendKeys(Integer.toString(randomNum(100, 999)));}
					}
					
				}
				
				
				 }
				 
		 
		 public void FillWAMedicaidPaymentAppendix() {

		CreateWAMedicaid_BTN.click();
		
		ReusableMethods.sleep(2000);
		
		
		try {
		driver.switchTo().alert().accept();
		ReusableMethods.sleep(2000);
		driver.switchTo().defaultContent();
		ReusableMethods.sleep(10000);
		}
		catch (Exception Ex)
		{
			System.out.println("No Alert present.");
		}
		
		ReusableMethods.sleep(25000);
		
		//ReusableMethods.selectUsingValue(OptumSigner_select, "Anil Facility");
		
	//filling site-location
		
	Iterator<WebElement>  iter2 = Non_stand_SiteLocation_TXT.iterator();
		System.out.println(Non_stand_SiteLocation_TXT.size());
		int n=Non_stand_SiteLocation_TXT.size();
		while(iter2.hasNext()) {
		    WebElement input_ele = iter2.next();
		
			if(input_ele.getAttribute("value").length() == 0) {
				if(input_ele.isDisplayed())	{
					input_ele.clear();;
					input_ele.sendKeys("1");}
			}
			
		}
		
		//filling rates
		
		Iterator<WebElement>  iter3 = Modifier_TXT.iterator();
		System.out.println(Modifier_TXT.size());
		
		while(iter3.hasNext()) {
		    WebElement input_ele = iter3.next();
		
			if(input_ele.getAttribute("value").length() == 0) {
				if(input_ele.isDisplayed())	{
					input_ele.clear();
					input_ele.sendKeys("WA");}
			}
			
		}
		
		Iterator<WebElement> iter = inputFields_TXT.iterator();
		
		while(iter.hasNext()) {
		    WebElement input_ele = iter.next();
		
			if(input_ele.getAttribute("value").length() == 0) {
				
				String rate = Integer.toString(randomNum(100, 999));
				if(input_ele.isDisplayed() )	{		
					input_ele.sendKeys(rate);
					}
			}
			
		}
		
		
		 }
		 
		 public void FillNEPaymentAppendix() {

				CreateNEMedicaid_BTN.click();
				
				ReusableMethods.sleep(2000);
				
				
				try {
				driver.switchTo().alert().accept();
				ReusableMethods.sleep(2000);
				driver.switchTo().defaultContent();
				ReusableMethods.sleep(10000);
				}
				catch (Exception Ex)
				{
					System.out.println("No Alert present.");
				}
				
				
				ReusableMethods.sleep(20000);
				
			Iterator<WebElement>  iter2 = Non_stand_SiteLocation_TXT.iterator();
				System.out.println(Non_stand_SiteLocation_TXT.size());
				
				while(iter2.hasNext()) {
				    WebElement input_ele = iter2.next();
				
					if(input_ele.getAttribute("value").length() == 0) {
						if(input_ele.isDisplayed())	{
							input_ele.clear();;
							input_ele.sendKeys("1");}
					}
					
				}
				
				
				
				Iterator<WebElement>  iter3 = NE_Modfier_TXT.iterator();
				System.out.println(NE_Modfier_TXT.size());
				
				while(iter3.hasNext()) {
				    WebElement input_ele = iter3.next();
				
					if(input_ele.getAttribute("value").length() == 0) {
						if(input_ele.isDisplayed())	{
							input_ele.clear();
							input_ele.sendKeys("NE");}
					}
					
				}
		
	}
	
	
		 public void FillKYPaymentAppendix() {

			 CreateKYMedicaid_BTN.click();
				
				ReusableMethods.sleep(2000);
				
				
				try {
				driver.switchTo().alert().accept();
				ReusableMethods.sleep(2000);
				driver.switchTo().defaultContent();
				ReusableMethods.sleep(10000);
				}
				catch (Exception Ex)
				{
					System.out.println("No Alert present.");
					ReusableMethods.sleep(10000);
				}
				
				
				
				Wait.untilPageLoadComplete(driver);
				
				Wait.waitUnitTextVisible(driver, header_text, "Non Standard Payment Appendix");
							
			Iterator<WebElement>  iter2 = Non_stand_SiteLocation_TXT.iterator();
				System.out.println(Non_stand_SiteLocation_TXT.size());
				
				while(iter2.hasNext()) {
				    WebElement input_ele = iter2.next();
				
					if(input_ele.getAttribute("value").length() == 0) {
						if(input_ele.isDisplayed())	{
							input_ele.clear();
							input_ele.sendKeys("1");}
					}
					
				}
				
				
				
				Iterator<WebElement>  iter3 = NE_Modfier_TXT.iterator();
				System.out.println(NE_Modfier_TXT.size());
				
				while(iter3.hasNext()) {
				    WebElement input_ele = iter3.next();
				
					if(input_ele.getAttribute("value").length() == 0) {
						if(input_ele.isDisplayed())	{
							input_ele.clear();
							input_ele.sendKeys("KY");}
					}
					
				}
		
	}
	

		 public void FillVAPaymentAppendix() {

		CreateVAMedicaid_BTN.click();
		
		ReusableMethods.sleep(2000);
		
		
		try {
		driver.switchTo().alert().accept();
		ReusableMethods.sleep(2000);
		driver.switchTo().defaultContent();
		ReusableMethods.sleep(10000);
		}
		catch (Exception Ex)
		{
			System.out.println("No Alert present.");
			ReusableMethods.sleep(10000);
		}
		
		Wait.untilPageLoadComplete(driver);
		
		Wait.waitUnitTextVisible(driver, header_text, "Non Standard Payment Appendix");
		Wait.waitUnitWebElementClickable(driver, add_BTN);
	
	//filling site-location
		
	Iterator<WebElement>  iter2 = Non_stand_SiteLocation_TXT.iterator();
		System.out.println(Non_stand_SiteLocation_TXT.size());
		
		while(iter2.hasNext()) {
		    WebElement input_ele = iter2.next();
		
			if(input_ele.getAttribute("value").length() == 0) {
				if(input_ele.isDisplayed())	{
					input_ele.clear();;
					input_ele.sendKeys("1");}
			}
			
		}
		
		//filling rates 
		
		Iterator<WebElement>  iter3 = perDiem_VA_txt.iterator();
		System.out.println(perDiem_VA_txt.size());
		
		while(iter3.hasNext()) {
		    WebElement input_ele = iter3.next();
		
			if(input_ele.getAttribute("value").length() == 0) {
				if(input_ele.isDisplayed())	{
					input_ele.clear();;
					input_ele.sendKeys(Integer.toString(randomNum(100, 999)));}
			}
			
		}
	
		 }
	
	
		 public void FillWIMedicaPaymentAppendix() {

			 CreateWIMedica_BTN.click();
				
				ReusableMethods.sleep(2000);
				
				
				try {
				driver.switchTo().alert().accept();
				ReusableMethods.sleep(2000);
				driver.switchTo().defaultContent();
				ReusableMethods.sleep(10000);
				}
				catch (Exception Ex)
				{
					System.out.println("No Alert present.");
					ReusableMethods.sleep(10000);
				}
				
				
				
				Wait.untilPageLoadComplete(driver);
				
				Wait.waitUnitTextVisible(driver, header_text, "Non Standard Payment Appendix");
				
				ReusableMethods.sleep(30000);
				
				System.out.println(inputFields_TXT.size());
				
				Iterator<WebElement> iter = inputFields_TXT.iterator();
		
				while(iter.hasNext()) {
				    WebElement input_ele = iter.next();
				
					if(input_ele.getAttribute("value").length() == 0) {
						
						String rate = Integer.toString(randomNum(100, 999));
						if(input_ele.isDisplayed() )	{		
							input_ele.sendKeys(rate);
							}
					}
					
				}
							
			Iterator<WebElement>  iter2 = Non_stand_SiteLocation_TXT.iterator();
				System.out.println(Non_stand_SiteLocation_TXT.size());
				
				while(iter2.hasNext()) {
				    WebElement input_ele = iter2.next();
				
					
						if(input_ele.isDisplayed())	{
							input_ele.clear();
							input_ele.sendKeys("1");}
					
					
				}
				
				
				
				Iterator<WebElement>  iter3 = Modifier_TXT.iterator();
				System.out.println(Modifier_TXT.size());
				
				while(iter3.hasNext()) {
				    WebElement input_ele = iter3.next();
				try {
					Integer.parseInt(input_ele.getAttribute("value"));
						if(input_ele.isDisplayed())	{
							input_ele.clear();
							input_ele.sendKeys("WI, MC");}
				}catch (NumberFormatException e)  
				{ 
					System.out.println("Not int + input_ele.getAttribute(\"value\")");
				} 
					
				}
				
				
				Iterator<WebElement>  iter4 = HCPC_TXT.iterator();
				System.out.println(HCPC_TXT.size());
				
				while(iter4.hasNext()) {
				    WebElement input_ele = iter4.next();
				
					if(input_ele.getAttribute("value").length() == 3) {
						if(input_ele.isDisplayed())	{
							input_ele.clear();
							input_ele.sendKeys("99217");}
					}
					
				}
		
	}
	
		 
		 public void FillUTMedicaidPaymentAppendix() {

			 CreateUTMedicaid_BTN.click();
				
				ReusableMethods.sleep(2000);
				
				
				try {
				driver.switchTo().alert().accept();
				ReusableMethods.sleep(2000);
				driver.switchTo().defaultContent();
				ReusableMethods.sleep(10000);
				}
				catch (Exception Ex)
				{
					System.out.println("No Alert present.");
					ReusableMethods.sleep(10000);
				}
				
				
				
				Wait.untilPageLoadComplete(driver);
				
				Wait.waitUnitTextVisible(driver, header_text, "Non Standard Payment Appendix");
				
				ReusableMethods.sleep(20000);
				
				System.out.println(inputFields_TXT.size());
				
				Iterator<WebElement> iter = inputFields_TXT.iterator();
		
				while(iter.hasNext()) {
				    WebElement input_ele = iter.next();
				
					if(input_ele.getAttribute("value").length() == 0) {
						
						String rate = Integer.toString(randomNum(100, 999));
						if(input_ele.isDisplayed() )	{		
							input_ele.sendKeys(rate);
							}
					}
					
				}
							
			
		
	}
		 
		 
		 
		 public void FillNYMedicaidPaymentAppendix() {

			 CreateNYMedicaid_BTN.click();
				
				ReusableMethods.sleep(2000);
				
				
				try {
				driver.switchTo().alert().accept();
				ReusableMethods.sleep(2000);
				driver.switchTo().defaultContent();
				ReusableMethods.sleep(10000);
				}
				catch (Exception Ex)
				{
					System.out.println("No Alert present.");
					ReusableMethods.sleep(10000);
				}
				
				
				
				Wait.untilPageLoadComplete(driver);
				
				Wait.waitUnitTextVisible(driver, header_text, "Non Standard Payment Appendix");
				
				ReusableMethods.sleep(20000);
				
				System.out.println(inputFields_TXT.size());
				
				Iterator<WebElement> iter = inputFields_TXT.iterator();
		
				while(iter.hasNext()) {
				    WebElement input_ele = iter.next();
				
					if(input_ele.getAttribute("value").length() == 0) {
						
						String rate = Integer.toString(randomNum(100, 999));
						if(input_ele.isDisplayed() )	{		
							input_ele.sendKeys(rate);
							}
					}
					
				}
				
				
				Iterator<WebElement>  iter3 = Modifier_TXT.iterator();
				System.out.println(Modifier_TXT.size());
				
				while(iter3.hasNext()) {
				    WebElement input_ele = iter3.next();
				
					if(input_ele.getAttribute("value").length() == 3) {
						if(input_ele.isDisplayed())	{
							input_ele.clear();
							input_ele.sendKeys("NY");}
					}
					
				}
				
				
				Iterator<WebElement>  iter4 = SL_TXT.iterator();
				System.out.println(SL_TXT.size());
				
				while(iter4.hasNext()) {
				    WebElement input_ele = iter4.next();
				
					if(input_ele.getAttribute("value").length() != 0) {
						if(input_ele.isDisplayed())	{
							input_ele.clear();
							input_ele.sendKeys("1");}
					}
					
				}
							
			
		
	}
		 
	
		 public void FillWIMedicaidPaymentAppendix() {

			 CreateWIMedicaid_BTN.click();
				
				ReusableMethods.sleep(2000);
				
				
				try {
				driver.switchTo().alert().accept();
				ReusableMethods.sleep(2000);
				driver.switchTo().defaultContent();
				ReusableMethods.sleep(10000);
				}
				catch (Exception Ex)
				{
					System.out.println("No Alert present.");
					ReusableMethods.sleep(10000);
				}
				
				
				
				Wait.untilPageLoadComplete(driver);
				
				Wait.waitUnitTextVisible(driver, header_text, "Non Standard Payment Appendix");
				
				ReusableMethods.sleep(20000);
				
				System.out.println(inputFields_TXT.size());
				
				Iterator<WebElement> iter = inputFields_TXT.iterator();
		
				while(iter.hasNext()) {
				    WebElement input_ele = iter.next();
				
					if(input_ele.getAttribute("value").length() == 0) {
						
						String rate = Integer.toString(randomNum(100, 999));
						if(input_ele.isDisplayed() )	{		
							input_ele.sendKeys(rate);
							}
					}
					
				}
							
			Iterator<WebElement>  iter2 = Non_stand_SiteLocation_TXT.iterator();
				System.out.println(Non_stand_SiteLocation_TXT.size());
				
				while(iter2.hasNext()) {
				    WebElement input_ele = iter2.next();
				
					
						if(input_ele.isDisplayed())	{
							input_ele.clear();
							input_ele.sendKeys("1");}
					
					
				}
				
				
				
				Iterator<WebElement>  iter3 = Modifier_TXT.iterator();
				System.out.println(Modifier_TXT.size());
				
				while(iter3.hasNext()) {
				    WebElement input_ele = iter3.next();
				try {
					Integer.parseInt(input_ele.getAttribute("value"));
						if(input_ele.isDisplayed())	{
							input_ele.clear();
							input_ele.sendKeys("WI, ID");}
				}catch (NumberFormatException e)  
				{ 
					System.out.println("Not int + input_ele.getAttribute(\"value\")");
				} 
					
				}
				
				
				Iterator<WebElement>  iter4 = HCPC_TXT.iterator();
				System.out.println(HCPC_TXT.size());
				
				while(iter4.hasNext()) {
				    WebElement input_ele = iter4.next();
				
					if(input_ele.getAttribute("value").length() == 3) {
						if(input_ele.isDisplayed())	{
							input_ele.clear();
							input_ele.sendKeys("99217");}
					}
					
				}
		
	}
		 
		 
		 public void FillKSPaymentAppendix() {

				CreateKSMedicaid_BTN.click();
				
				ReusableMethods.sleep(2000);
				
				
				try {
				driver.switchTo().alert().accept();
				ReusableMethods.sleep(2000);
				driver.switchTo().defaultContent();
				ReusableMethods.sleep(10000);
				}
				catch (Exception Ex)
				{
					System.out.println("No Alert present.");
				}
				
				ReusableMethods.sleep(25000);
				
				//ReusableMethods.selectUsingValue(OptumSigner_select, "Anil Facility");
				
			//filling site-location
				
			Iterator<WebElement>  iter2 = Non_stand_SiteLocation_TXT.iterator();
				System.out.println(Non_stand_SiteLocation_TXT.size());
				int n=Non_stand_SiteLocation_TXT.size();
				while(iter2.hasNext()) {
				    WebElement input_ele = iter2.next();
				
					if(input_ele.getAttribute("value").length() == 0) {
						if(input_ele.isDisplayed())	{
							input_ele.clear();;
							input_ele.sendKeys("1");}
					}
					
				}
				
				//filling rates
				
				Iterator<WebElement>  iter3 = KSperDiem_TXT.iterator();
				System.out.println(KSperDiem_TXT.size());
				
				while(iter3.hasNext()) {
				    WebElement input_ele = iter3.next();
				
					if(input_ele.getAttribute("value").length() == 0) {
						if(input_ele.isDisplayed())	{
							input_ele.clear();
							input_ele.sendKeys(Integer.toString(randomNum(100, 999)));}
					}
					
				}
				
				
				 }
		 
		 
		 
	
	public void scrollDownVertically() {
        try {
            executeScript("window.scrollTo(0, document.body.scrollHeight)");
        }
        catch (Exception e){
           
        }
    }
	
	public Object executeScript(String script, Object... args) {
        JavascriptExecutor exe = (JavascriptExecutor) driver;
        
        return exe.executeScript(script, args);
    }
	
	public static int randomNum(int min, int max) {
		int number;
		Random rn = new Random();
		int range = (max - min) + 1;
		number = rn.nextInt(range) + min;
		
		return number;
		
	}

	
}
