package FRNF.Pages;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import FNRFutil.ConfigFileReader;
import FNRFutil.ReusableMethods;
import FNRFutil.Wait;

public class PracticeLocations {
	WebDriver driver;
	ConfigFileReader configFileReader = null;
	ReusableMethods ReusableMethods = null;

	public PracticeLocations(WebDriver driver) {
		this.driver = driver;
		configFileReader = new ConfigFileReader();
		ReusableMethods = new ReusableMethods();
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//button[text()='Add Location - SUD & MH']")
	public static WebElement AddLocationSUDMH_btn;

	@FindBy(xpath = "//*[text()='SUD & MH Location']")
	public static WebElement SUDMHLocation;

	@FindBy(xpath = "//*[text()='Address Line 1']//following::input[1]")
	public static WebElement AddressLine1_TXT;

	@FindBy(xpath = "//*[text()='Address Line 2']//following::input[1]")
	public static WebElement AddressLine2_TXT;

	@FindBy(xpath = "//*[text()='City']//following::input[1]")
	public static WebElement City_TXT;

	@FindBy(xpath = "//*[text()='State']//following::button[1]")
	public static WebElement State_TXT;

	@FindBy(xpath = "//*[text()='Zip']//following::input[1]")
	public static WebElement Zip_TXT;

	@FindBy(xpath = "//*[text()='Phone']//following::input[1]")
	public static WebElement Phone_TXT;

	@FindBy(xpath = "//*[text()='Secure Fax?']//following::button[1]")
	public static WebElement SecureFax_CHK;

	@FindBy(xpath = "//*[text()='Provide Secure Fax']//following::input[1]")
	public static WebElement ProvideSecureFax_TXT;

	@FindBy(xpath = "//*[text()='Number of IP Beds (SUD)']//following::input[1]")
	public static WebElement NumberofIPBeds_TXT;

	@FindBy(xpath = "//*[text()='Location is SAMHSA-certified as Medication-assisted treatment (MAT) Center?']//following::button[1]")
	public static WebElement Location_btn;

	@FindBy(xpath = "//*[text()='Wheelchair Accessibility for SUD Location']//following::button[1]")
	public static WebElement Wheelchair_btn;

	@FindBy(xpath = "//*[text()='Location is not SAMHSA-certified as a MAT Center but employs providers who completed the MAT waiver training']//following::button[1]")
	public static WebElement waivertraining_btn;

	@FindBy(xpath = "//*[text()='Location provides Detox Services in which controlled substance are used during treatment?']//following::button[1]")
	public static WebElement Detox_TXT;

	@FindBy(xpath = "//*[text()='Number of IP Beds (MH) at Location']//following::input[1]")
	public static WebElement NumberOfIPBeds_btn;

	@FindBy(xpath = "//*[text()='Number of Medicare Acute IP Beds (MH)']//following::input[1]")
	public static WebElement MedicareAcuteIPBeds_btn;

	@FindBy(xpath = "//*[text()='Wheelchair Accessibility for MH Location']//following::button[1]")
	public static WebElement WheelchairAccessibility_btn;

	@FindBy(xpath = "//*[text()='AK']")
	public static WebElement CA_btn;

	@FindBy(xpath = "//*[text()='Yes']")
	public static WebElement FAX_Yes_btn;

	@FindBy(xpath = "//*[text()='Yes']")
	public static WebElement Wheel_Yes_btn;

	@FindBy(xpath = "//*[text()='Yes']")
	public static WebElement Location_Yes_btn;

	@FindBy(xpath = "//*[text()='Yes']")
	public static WebElement Waiver_Yes_btn;

	@FindBy(xpath = "//*[text()='Yes']")
	public static WebElement Detox_Yes_btn;

	@FindBy(xpath = "//*[text()='Yes']")
	public static WebElement WheelchairAccess_Yes_btn;

	@FindBy(xpath = "//button[text()='Save']")
	public static WebElement Save_btn;

	@FindBy(xpath = "//button[text()='Save & Continue']")
	public static WebElement SaveCountinue_btn;

	@FindBy(xpath = "//*[text()='Substance Use Disorder']//following::tr[1]/td[3]/lightning-input/div/span/label")
	public static WebElement AdultASAM;

	@FindBy(xpath = "//*[text()='Substance Use Disorder']//following::tr[1]/td[4]/lightning-input/div/span/label")
	public static WebElement GeriatricASAM;

	@FindBy(xpath = "//*[text()='Substance Use Disorder']//following::tr[1]/td[5]/lightning-input/div/span/label")
	public static WebElement AdolescentASAM;

	@FindBy(xpath = "//*[text()='Substance Use Disorder']//following::tr[2]/td[3]/lightning-input/div/span/label")
	public static WebElement AdultASAM37;

	@FindBy(xpath = "//*[text()='Substance Use Disorder']//following::tr[2]/td[4]/lightning-input/div/span/label")
	public static WebElement GeriatricASAM37;

	@FindBy(xpath = "//*[text()='Substance Use Disorder']//following::tr[2]/td[5]/lightning-input/div/span/label")
	public static WebElement AdolescentASAM37;

	@FindBy(xpath = "//*[text()='Substance Use Disorder']//following::tr[3]/td[3]/lightning-input/div/span/label")
	public static WebElement subAdultASAM37;

	@FindBy(xpath = "//*[text()='Substance Use Disorder']//following::tr[3]/td[4]/lightning-input/div/span/label")
	public static WebElement subGeriatricASAM37;

	@FindBy(xpath = "//*[text()='Substance Use Disorder']//following::tr[3]/td[5]/lightning-input/div/span/label")
	public static WebElement subAdolescentASAM37;

	@FindBy(xpath = "//*[text()='Substance Use Disorder']//following::tr[4]/td[3]/lightning-input/div/span/label")
	public static WebElement subAdultASAM35;

	@FindBy(xpath = "//*[text()='Substance Use Disorder']//following::tr[4]/td[4]/lightning-input/div/span/label")
	public static WebElement subGeriatricASAM35;

	@FindBy(xpath = "//*[text()='Substance Use Disorder']//following::tr[4]/td[5]/lightning-input/div/span/label")
	public static WebElement subAdolescentASAM35;

	@FindBy(xpath = "//*[text()='Substance Use Disorder']//following::tr[5]/td[3]/lightning-input/div/span/label")
	public static WebElement AdultASAM25;

	@FindBy(xpath = "//*[text()='Substance Use Disorder']//following::tr[5]/td[4]/lightning-input/div/span/label")
	public static WebElement GeriatricASAM25;

	@FindBy(xpath = "//*[text()='Substance Use Disorder']//following::tr[5]/td[5]/lightning-input/div/span/label")
	public static WebElement AdolescentASAM25;

	@FindBy(xpath = "//*[text()='Substance Use Disorder']//following::tr[6]/td[3]/lightning-input/div/span/label")
	public static WebElement AdultASAM21;

	@FindBy(xpath = "//*[text()='Substance Use Disorder']//following::tr[6]/td[4]/lightning-input/div/span/label")
	public static WebElement GeriatricASAM21;

	@FindBy(xpath = "//*[text()='Substance Use Disorder']//following::tr[6]/td[5]/lightning-input/div/span/label")
	public static WebElement AdolescentASAM21;

	@FindBy(xpath = "//*[text()='Substance Use Disorder']//following::tr[7]/td[3]/lightning-input/div/span/label")
	public static WebElement AdultASAM1wm;

	@FindBy(xpath = "//*[text()='Substance Use Disorder']//following::tr[7]/td[4]/lightning-input/div/span/label")
	public static WebElement GeriatricASAM1wm;

	@FindBy(xpath = "//*[text()='Substance Use Disorder']//following::tr[7]/td[5]/lightning-input/div/span/label")
	public static WebElement AdolescentASAM1wm;

	@FindBy(xpath = "//*[text()='Substance Use Disorder']//following::tr[8]/td[3]/lightning-input/div/span/label")
	public static WebElement AdultASAM1;

	@FindBy(xpath = "//*[text()='Substance Use Disorder']//following::tr[8]/td[4]/lightning-input/div/span/label")
	public static WebElement GeriatricASAM1;

	@FindBy(xpath = "//*[text()='Substance Use Disorder']//following::tr[8]/td[5]/lightning-input/div/span/label")
	public static WebElement AdolescentASAM1;


	@FindBy(xpath = "//*[text()='Mental Health']//following::tr[1]/td[2]/lightning-input/div/span/label")
	public static WebElement iplockedadult;

	@FindBy(xpath = "//*[text()='Mental Health']//following::tr[1]/td[3]/lightning-input/div/span/label")
	public static WebElement iplockedGeriatric;

	@FindBy(xpath = "//*[text()='Mental Health']//following::tr[1]/td[4]/lightning-input/div/span/label")
	public static WebElement iplockedAdolescent;

	@FindBy(xpath = "//*[text()='Mental Health']//following::tr[1]/td[5]/lightning-input/div/span/label")
	public static WebElement iplockedChild;

	@FindBy(xpath = "//*[text()='Mental Health']//following::tr[2]/td[2]/lightning-input/div/span/label")
	public static WebElement opendadult;

	@FindBy(xpath = "//*[text()='Mental Health']//following::tr[2]/td[3]/lightning-input/div/span/label")
	public static WebElement openGeriatric;

	@FindBy(xpath = "//*[text()='Mental Health']//following::tr[2]/td[4]/lightning-input/div/span/label")
	public static WebElement openAdolescent;

	@FindBy(xpath = "//*[text()='Mental Health']//following::tr[2]/td[5]/lightning-input/div/span/label")
	public static WebElement openChild;

	@FindBy(xpath = "//*[text()='Mental Health']//following::tr[3]/td[2]/lightning-input/div/span/label")
	public static WebElement resadult;

	@FindBy(xpath = "//*[text()='Mental Health']//following::tr[3]/td[3]/lightning-input/div/span/label")
	public static WebElement resGeriatric;

	@FindBy(xpath = "//*[text()='Mental Health']//following::tr[3]/td[4]/lightning-input/div/span/label")
	public static WebElement resAdolescent;

	@FindBy(xpath = "//*[text()='Mental Health']//following::tr[3]/td[5]/lightning-input/div/span/label")
	public static WebElement resChild;

	@FindBy(xpath = "//*[text()='Mental Health']//following::tr[4]/td[2]/lightning-input/div/span/label")
	public static WebElement PHPadult;

	@FindBy(xpath = "//*[text()='Mental Health']//following::tr[4]/td[3]/lightning-input/div/span/label")
	public static WebElement PHPGeriatric;

	@FindBy(xpath = "//*[text()='Mental Health']//following::tr[4]/td[4]/lightning-input/div/span/label")
	public static WebElement PHPAdolescent;

	@FindBy(xpath = "//*[text()='Mental Health']//following::tr[4]/td[5]/lightning-input/div/span/label")
	public static WebElement PHPChild;

	@FindBy(xpath = "//*[text()='Mental Health']//following::tr[5]/td[2]/lightning-input/div/span/label")
	public static WebElement IOPadult;

	@FindBy(xpath = "//*[text()='Mental Health']//following::tr[5]/td[3]/lightning-input/div/span/label")
	public static WebElement IOPGeriatric;

	@FindBy(xpath = "//*[text()='Mental Health']//following::tr[5]/td[4]/lightning-input/div/span/label")
	public static WebElement IOPAdolescent;

	@FindBy(xpath = "//*[text()='Mental Health']//following::tr[5]/td[5]/lightning-input/div/span/label")
	public static WebElement IOPChild;
	
	
	
	
	@FindBy(xpath = "//input[contains(@type, 'checkbox')]")
	public static List<WebElement> chks;

	
	
	
	@FindBy(xpath = "//td[normalize-space()='Other']/following::textarea[1]")
	public static WebElement otherAdult_TXT;
	
	@FindBy(xpath = "//td[normalize-space()='Other']/following::textarea[2]")
	public static WebElement otherGeriatric_TXT;;
	
	@FindBy(xpath = "//td[normalize-space()='Other']/following::textarea[3]")
	public static WebElement otherAdolscent_TXT;;
	
	public void Addlocation(Map<String, String> xl) {
		Wait.waitUnitWebElementVisible(driver, AddLocationSUDMH_btn);
		Wait.waitUnitWebElementClickable(driver, AddLocationSUDMH_btn);
		AddLocationSUDMH_btn.click();
		Wait.waitUnitWebElementVisible(driver, SUDMHLocation);
		SUDMHLocation.click();

		Wait.waitUnitWebElementVisible(driver, PracticeLocations.AddressLine1_TXT);
		FNRFutil.ReusableMethods.enterTextByClearValue(PracticeLocations.AddressLine1_TXT, xl.get("AddressLine1"), "AddressLine1", driver);
		FNRFutil.ReusableMethods.enterTextByClearValue(PracticeLocations.AddressLine2_TXT, xl.get("AddressLine2"), "AddressLine2", driver);
		FNRFutil.ReusableMethods.enterTextByClearValue(PracticeLocations.City_TXT, xl.get("City"), "City", driver);
		FNRFutil.ReusableMethods.selectState(PracticeLocations.State_TXT, driver, xl.get("State"), "State");
		State_TXT.sendKeys(Keys.ENTER);
		FNRFutil.ReusableMethods.enterTextByClearValue(PracticeLocations.Zip_TXT, xl.get("Zip"), "Zip", driver);
		FNRFutil.ReusableMethods.enterTextByClearValue(PracticeLocations.Phone_TXT, xl.get("Phone"), "Phone", driver);
		
		ReusableMethods.javascriptclick(SecureFax_CHK, driver);
		Wait.waitUnitWebElementVisible(driver, FAX_Yes_btn);
		ReusableMethods.javascriptclick(FAX_Yes_btn, driver);
		ProvideSecureFax_TXT.sendKeys("(939)056-3471");
		NumberofIPBeds_TXT.sendKeys("1");
		ReusableMethods.javascriptclick(Wheelchair_btn, driver);
		ReusableMethods.clickdropdown(0);
		ReusableMethods.javascriptclick(Location_btn, driver);
		ReusableMethods.clickdropdown(0);

		ReusableMethods.javascriptclick(waivertraining_btn, driver);
		ReusableMethods.clickdropdown(0);

		ReusableMethods.javascriptclick(Detox_TXT, driver);
		ReusableMethods.clickdropdown(0);

		NumberOfIPBeds_btn.sendKeys("1");
		MedicareAcuteIPBeds_btn.sendKeys("1");

		ReusableMethods.javascriptclick(WheelchairAccessibility_btn, driver);
		ReusableMethods.clickdropdown(0);
		ReusableMethods.sleep(2000);
		
		for (WebElement chk : chks) {
			//ReusableMethods.sleep(2000);
			//chk.click();
			ReusableMethods.javascriptclick(chk, driver);
			
		}
		ReusableMethods.sleep(2000);
//		otherAdult_TXT.sendKeys("This is for the testing the value in Adult");
//		otherGeriatric_TXT.sendKeys("This is for the testing the value in Geriatric");
//		otherAdolscent_TXT.sendKeys("This is for the testing the value in Adolscent");
//		setVal(otherAdult_TXT, "This is for the testing the value in Adult");
//		ReusableMethods.sleep(2000);
//		setVal(otherGeriatric_TXT, "This is for the testing the value in Geriatric");
//		ReusableMethods.sleep(2000);
//		setVal(otherAdolscent_TXT, "This is for the testing the value in Adolscent");
		/*
		try {
			LevelsofCare.ECT1_CHK.sendKeys("Both");
			Thread.sleep(1500);
			LevelsofCare.ECT1_CHK.sendKeys(Keys.ENTER);
			LevelsofCare.ECT2_CHK.sendKeys("Both");
			Thread.sleep(1000);
			LevelsofCare.ECT2_CHK.sendKeys(Keys.ENTER);
			LevelsofCare.ECT3_CHK.sendKeys("Both");
			Thread.sleep(1000);
			LevelsofCare.ECT3_CHK.sendKeys(Keys.ENTER);
			LevelsofCare.ECT4_CHK.sendKeys("Both");
			Thread.sleep(1000);
			LevelsofCare.ECT4_CHK.sendKeys(Keys.ENTER);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		*/
		Wait.waitUnitWebElementVisible(driver, Save_btn);
		ReusableMethods.javascriptclick(Save_btn, driver);
		
		ReusableMethods.sleep(10000);
		
		Wait.waitUnitWebElementVisible(driver, SUDMHLocation);
		SUDMHLocation.click();
		ReusableMethods.sleep(10000);
		
		
		ReusableMethods.javascriptclick(SaveCountinue_btn, driver);

	}
	
	public void setVal(WebElement element, String str) {
	JavascriptExecutor js=(JavascriptExecutor)driver;
	js.executeScript("arguments[0].setAttribute('value',arguments[1]);",element,str);
	}
}


/*
ReusableMethods.Checkelementclick(LevelsofCare.IPLockedAdult_CHK,  "IPLockedAdult", driver);
ReusableMethods.Checkelementclick(LevelsofCare.IPLockedGeriatric_CHK,  "IPLockedGeriatric", driver);
ReusableMethods.Checkelementclick(LevelsofCare.IPLockedAdolescent_CHK, "IPLockedAdolescent", driver);
ReusableMethods.Checkelementclick(LevelsofCare.IPLockedChild_CHK, "IPLockedChild", driver);







		ReusableMethods.Checkelementclick(LevelsofCare.IPLockedAdult_CHK,  "IPLockedAdult", driver);
		ReusableMethods.Checkelementclick(LevelsofCare.IPLockedGeriatric_CHK,  "IPLockedGeriatric", driver);
		ReusableMethods.Checkelementclick(LevelsofCare.IPLockedAdolescent_CHK, "IPLockedAdolescent", driver);
		ReusableMethods.Checkelementclick(LevelsofCare.IPLockedChild_CHK, "IPLockedChild", driver);
		
		ReusableMethods.Checkelementclick(LevelsofCare.IPOpenAdolescent_CHK,  "IPOpenAdolescent", driver);
		ReusableMethods.Checkelementclick(LevelsofCare.IPOpenGeriatric_CHK,  "IPOpenGeriatric", driver);
		ReusableMethods.Checkelementclick(LevelsofCare.IPOpenAdolescent_CHK,  "IPOpenAdolescent", driver);
		ReusableMethods.Checkelementclick(LevelsofCare.IPOpenGeriatric_CHK,  "IPOpenGeriatric", driver);


		ReusableMethods.Checkelementclick(LevelsofCare.ResidentialAdolescent_CHK,  "ResidentialAdolescent", driver);
		ReusableMethods.Checkelementclick(LevelsofCare.ResidentialAdult_CHK,  "ResidentialAdult", driver);
		ReusableMethods.Checkelementclick(LevelsofCare.ResidentialChild_CHK,  "ResidentialChild", driver);
		ReusableMethods.Checkelementclick(LevelsofCare.ResidentialGeriatric_CHK,  "ResidentialGeriatric", driver);


		ReusableMethods.Checkelementclick(LevelsofCare.PHPAdolescent_CHK,  "PHPAdolescent", driver);
		ReusableMethods.Checkelementclick(LevelsofCare.PHPAdult_CHK,  "PHPAdult", driver);
		ReusableMethods.Checkelementclick(LevelsofCare.PHPChild_CHK,  "PHPChild", driver);
		ReusableMethods.Checkelementclick(LevelsofCare.PHPGeriatric_CHK,  "PHPGeriatric", driver);

		ReusableMethods.Checkelementclick(LevelsofCare.IOPAdolescent_CHK,  "IOPAdolescent", driver);
		ReusableMethods.Checkelementclick(LevelsofCare.IOPAdult_CHK,  "IOPAdult", driver);
		ReusableMethods.Checkelementclick(LevelsofCare.IOPChild_CHK,  "IOPChild", driver);
		ReusableMethods.Checkelementclick(LevelsofCare.IOPGeriatric_CHK,  "IOPGeriatric", driver);


		ReusableMethods.Checkelementclick(LevelsofCare.CrisisServicesAdolescent_CHK,  "CrisisServicesAdolescent", driver);
		ReusableMethods.Checkelementclick(LevelsofCare.CrisisServicesAdult_CHK,  "CrisisServicesAdult", driver);
		ReusableMethods.Checkelementclick(LevelsofCare.CrisisServicesChild_CHK,  "CrisisServicesChild", driver);
		ReusableMethods.Checkelementclick(LevelsofCare.CrisisServicesGeriatric_CHK, "CrisisServicesGeriatric", driver);


		ReusableMethods.Checkelementclick(LevelsofCare.ASAM37WMAdolescent_CHK,  "ASAM37WMAdolescent", driver);
		ReusableMethods.Checkelementclick(LevelsofCare.ASAM37WMAdult_CHK,  "ASAM37WMAdult", driver);
		ReusableMethods.Checkelementclick(LevelsofCare.ASAM37WMGeriatric_CHK,  "ASAM37WMGeriatric", driver);


		ReusableMethods.Checkelementclick(LevelsofCare.ASAM4Adolescent_CHK,  "ASAM4Adolescent", driver);
		ReusableMethods.Checkelementclick(LevelsofCare.ASAM4Adult_CHK,  "ASAM4Adult", driver);
		ReusableMethods.Checkelementclick(LevelsofCare.ASAM4Geriatric_CHK, "ASAM4Geriatric", driver);
		
		*/
