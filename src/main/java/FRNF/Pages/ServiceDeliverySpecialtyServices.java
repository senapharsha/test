package FRNF.Pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import FNRFutil.ConfigFileReader;
import FNRFutil.ReusableMethods;
import FNRFutil.Wait;

public class ServiceDeliverySpecialtyServices {
	WebDriver driver;
	ConfigFileReader configFileReader = null;
	ReusableMethods ReusableMethods = null;

	public ServiceDeliverySpecialtyServices(WebDriver driver) {
		this.driver = driver;
		configFileReader = new ConfigFileReader();
		ReusableMethods = new ReusableMethods();
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//*[text()='If Medically Managed Intensive Inpatient (ASAM 4)is offered at Facility, please select, the physical location of beds']//following::button[1]")
	private WebElement twolevelcare;
	
	@FindBy(xpath = "//*[text()='If Medically Managed Intensive Inpatient (ASAM 4)is offered at Facility, please select, the physical location of beds']//following::button[2]")
	private WebElement IOPASAM21;
	
	@FindBy(xpath = "//*[text()='If Medically Managed Intensive Inpatient (ASAM 4)is offered at Facility, please select, the physical location of beds']//following::button[3]")
	private WebElement MultilingualAssistanceAvailable;

	@FindBy(xpath = "//*[text()='Clinical Program Description-including any specialty program descriptions and hours per day/ days per week']//following::label[1]")
	private WebElement specialty;

	@FindBy(xpath = "//*[text()='Daily Program Schedule(s) – include an hour-by-hour schedule showing a patient’s daily treatment for each level of care you provide.']//following::label[1]")
	private WebElement schedule;

	@FindBy(xpath = "//*[text()='MAT Waiver for Individual Provider(s)']//following::label[1]")
	private WebElement MAT;
	
	
	@FindBy(xpath = "//div[contains(text(), 'Dual Diagnosis Services')]/following::input[1]")
	private WebElement DualDiagnosisServices_select;
	
	@FindBy(xpath = "//div[contains(text(), 'Emergency Room Services')]/following::input[1]")
	private WebElement crisisERServices_select;
	
	@FindBy(xpath = "//div[contains(text(), 'Twenty-three (23) Hour Crisis Observation')]/following::input[1]")
	private WebElement crisisObservation_select;
	
	@FindBy(xpath = "//div[contains(text(), 'Mobile Crisis Stabilization Available?')]/following::input[1]")
	private WebElement crisisStabilization_select;
	
	@FindBy(xpath = "/html/body/div[7]/div/div[2]/div/div[3]/div/span[2]/div/button")
	private WebElement Done_btn1;
	
	@FindBy(xpath = "//button[text()='Save & Continue']")
	private WebElement SaveCountinue_btn;
	
	@FindBy(xpath = "//*[text()='No']")
	private WebElement No_btn;

	public void enterservicedelivery() {
		
	/*	Wait.waitUnitWebElementVisible(driver, twolevelcare);
		ReusableMethods.javascriptclick(twolevelcare, driver);
		Wait.waitUnitWebElementVisible(driver, No_btn);
		ReusableMethods.javascriptclick(No_btn, driver);
		
		
		Wait.waitUnitWebElementVisible(driver, IOPASAM21);
		ReusableMethods.javascriptclick(IOPASAM21, driver);
		Wait.waitUnitWebElementVisible(driver, No_btn);
		ReusableMethods.javascriptclick(No_btn, driver);
		
		Wait.waitUnitWebElementVisible(driver, MultilingualAssistanceAvailable);
		ReusableMethods.javascriptclick(MultilingualAssistanceAvailable, driver);
		Wait.waitUnitWebElementVisible(driver, No_btn);
		ReusableMethods.javascriptclick(No_btn, driver);*/
		
		try {
			Thread.sleep(2000);

			Wait.waitUnitWebElementVisible(driver, twolevelcare);

			List<WebElement> list = driver.findElements(By.xpath("//button[@class='slds-combobox__input slds-input_faux']"));
			for (int i = 0; i < list.size(); i++) {
				Thread.sleep(1000);
				WebElement ele = list.get(i);
				ele.sendKeys("No");
				Thread.sleep(1000);
				ele.sendKeys(Keys.ENTER);

			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*ReusableMethods.sleep(1000);
		crisisERServices_select.sendKeys("Yes");
		ReusableMethods.sleep(1000);
		crisisERServices_select.sendKeys(Keys.ENTER);
		
		crisisObservation_select.sendKeys("Yes");
		ReusableMethods.sleep(1000);
		crisisObservation_select.sendKeys(Keys.ENTER);
		
		crisisStabilization_select.sendKeys("Yes");
		ReusableMethods.sleep(1000);
		crisisStabilization_select.sendKeys(Keys.ENTER);
		
		DualDiagnosisServices_select.sendKeys("Yes");
		ReusableMethods.sleep(1000);
		DualDiagnosisServices_select.sendKeys(Keys.ENTER);*/
		
		ReusableMethods.javascriptclick(specialty, driver);

		ReusableMethods.uploadFile("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, Done_btn1);
		ReusableMethods.javascriptclick(Done_btn1, driver);

		ReusableMethods.javascriptclick(schedule, driver);

		ReusableMethods.uploadFile("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, Done_btn1);
		ReusableMethods.javascriptclick(Done_btn1, driver);

		ReusableMethods.javascriptclick(MAT, driver);

		ReusableMethods.uploadFile("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, Done_btn1);
		ReusableMethods.javascriptclick(Done_btn1, driver);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ReusableMethods.javascriptclick(SaveCountinue_btn, driver);
	}

}
