package FRNF.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import FNRFutil.ConfigFileReader;
import FNRFutil.ReusableMethods;
import FNRFutil.Wait;

public class accredation {

	WebDriver driver;
	ConfigFileReader configFileReader = null;
	ReusableMethods ReusableMethods = null;

	public accredation(WebDriver driver) {
		this.driver = driver;
		configFileReader = new ConfigFileReader();
		ReusableMethods = new ReusableMethods();
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//*[text()='The Joint Commission']//following::label[1]")
	private WebElement TheJointCommission_TXT;

	@FindBy(xpath = "//*[text()='The Joint Commission']//following::input[2]")
	private WebElement TheJointCommission_1_TXT;

	@FindBy(xpath = "//*[text()='The Joint Commission']//following::input[3]")
	private WebElement TheJointCommission_2_TXT;

	@FindBy(xpath = "//*[text()='Commission on Accreditation of Rehabilitation Facilities (CARF)']//following::label[1]")
	private WebElement CommissiononAccreditationofRehabilitationFacilities_TXT;

	@FindBy(xpath = "//*[text()='Commission on Accreditation of Rehabilitation Facilities (CARF)']//following::input[2]")
	private WebElement CommissiononAccreditationofRehabilitationFacilities1_TXT;

	@FindBy(xpath = "//*[text()='Commission on Accreditation of Rehabilitation Facilities (CARF)']//following::input[3]")
	private WebElement CommissiononAccreditationofRehabilitationFacilities2_TXT;

	@FindBy(xpath = "//*[text()='American Osteopathic Association (AOA)']//following::label[1]")
	private WebElement AmericanOsteopathicAssociation_2_TXT;

	@FindBy(xpath = "//*[text()='American Osteopathic Association (AOA)']//following::input[2]")
	private WebElement AmericanOsteopathicAssociation_1_TXT;

	@FindBy(xpath = "//*[text()='American Osteopathic Association (AOA)']//following::input[3]")
	private WebElement AmericanOsteopathicAssociation_3_TXT;

	@FindBy(xpath = "//*[text()='Council on Accreditation (COA)']//following::label[1]")
	private WebElement CouncilonAccreditation_2_TXT;

	@FindBy(xpath = "//*[text()='Council on Accreditation (COA)']//following::input[2]")
	private WebElement CouncilonAccreditation__TXT;

	@FindBy(xpath = "//*[text()='Council on Accreditation (COA)']//following::input[3]")
	private WebElement CouncilonAccreditation_3_TXT;

	@FindBy(xpath = "//*[text()='Community Health Accreditation Program (CHAP)']//following::label[1]")
	private WebElement CommunityHealthAccreditationProgram_2_TXT;

	@FindBy(xpath = "//*[text()='Community Health Accreditation Program (CHAP)']//following::input[2]")
	private WebElement CommunityHealthAccreditationProgram__TXT;

	@FindBy(xpath = "//*[text()='Community Health Accreditation Program (CHAP)']//following::input[3]")
	private WebElement CommunityHealthAccreditationProgram_3_TXT;

	@FindBy(xpath = "//*[text()='American Association for Ambulatory Health Care (AAAHC)']//following::label[1]")
	private WebElement AmericanAssociationforAmbulatoryHealthCare_2_TXT;

	@FindBy(xpath = "//*[text()='American Association for Ambulatory Health Care (AAAHC)']//following::input[2]")
	private WebElement AmericanAssociationforAmbulatoryHealthCare__TXT;

	@FindBy(xpath = "//*[text()='American Association for Ambulatory Health Care (AAAHC)']//following::input[3]")
	private WebElement AmericanAssociationforAmbulatoryHealthCare_3_TXT;

	@FindBy(xpath = "//*[text()='Critical Access Hospitals (CAH)']//following::label[1]")
	private WebElement CriticalAccessHospitals_2_TXT;

	@FindBy(xpath = "//*[text()='Critical Access Hospitals (CAH)']//following::input[2]")
	private WebElement CriticalAccessHospitals__TXT;

	@FindBy(xpath = "//*[text()='Critical Access Hospitals (CAH)']//following::input[3]")
	private WebElement CriticalAccessHospitals_3_TXT;

	@FindBy(xpath = "//*[text()='Healthcare Facilities Accreditation Program (HFAP, through AOA)']//following::label[1]")
	private WebElement HealthcareFacilitiesAccreditationProgram_2_TXT;

	@FindBy(xpath = "//*[text()='Healthcare Facilities Accreditation Program (HFAP, through AOA)']//following::input[2]")
	private WebElement HealthcareFacilitiesAccreditationProgram__TXT;

	@FindBy(xpath = "//*[text()='Healthcare Facilities Accreditation Program (HFAP, through AOA)']//following::input[3]")
	private WebElement HealthcareFacilitiesAccreditationProgram_3_TXT;

	@FindBy(xpath = "/html/body/div[3]/div[2]/div/div[2]/div/div/c-f-n-r-f_-verification-page/lightning-record-edit-form/form/slot/c-fnrf-m-a-i-n/lightning-card[2]/article/div[2]/slot/lightning-layout/slot/div/lightning-layout-item/slot/lightning-record-edit-form/form/slot/c-accred/lightning-record-edit-form/form/slot/div[1]/table/tbody/tr[9]/td[2]/div/lightning-input/div/span/label")
	private WebElement NationalIntegratedAccreditationforHealthcareOrganizations_2_TXT;

	@FindBy(xpath = "//*[text()='Accreditation Commissions for Healthcare (ACHC)']//following::label[1]")
	private WebElement AccreditationCommissionsforHealthcare_2_TXT;

	@FindBy(xpath = "//*[text()='Accreditation Commissions for Healthcare (ACHC)']//following::input[2]")
	private WebElement AccreditationCommissionsforHealthcare__TXT;

	@FindBy(xpath = "//*[text()='Accreditation Commissions for Healthcare (ACHC)']//following::input[3]")
	private WebElement AccreditationCommissionsforHealthcare_3_TXT;

	@FindBy(xpath = "//*[text()='ASAM Level 3.1 (Adult)']//preceding::label[1]")
	private WebElement ASAMLevel3_TXT;

	@FindBy(xpath = "//*[text()='ASAM Level 3.5 (Adult)']//preceding::label[1]")
	private WebElement ASAMLevel35_TXT;

	@FindBy(xpath = "//*[text()='ASAM Level 3.7 (Adult)']//preceding::label[1]")
	private WebElement ASAMLevel37_TXT;

	@FindBy(xpath = "/html/body/div[7]/div/div[2]/div/div[3]/div/span[2]/div/button")
	private WebElement AccreditationStatusTheJointCommissionDone_btn;
	@FindBy(xpath = "//*[text()='Accreditation Status The Joint Commission']//following::label[1]")
	private WebElement AccreditationStatusTheJointCommission_TXT;

	@FindBy(xpath = "/html/body/div[7]/div/div[2]/div/div[3]/div/span[2]/div/button")
	private WebElement AccreditationStatusCARFDone_btn;
	@FindBy(xpath = "//*[text()='Accreditation Status CARF']//following::label[1]")
	private WebElement AccreditationStatusCARF_TXT;

	@FindBy(xpath = "/html/body/div[7]/div/div[2]/div/div[3]/div/span[2]/div/button")
	private WebElement AccreditationStatusAOADone_btn;
	@FindBy(xpath = "//*[text()='Accreditation Status AOA']//following::label[1]")
	private WebElement AccreditationStatusAOA_TXT;

	@FindBy(xpath = "/html/body/div[7]/div/div[2]/div/div[3]/div/span[2]/div/button")
	private WebElement AccreditationStatusCOADone_btn;
	@FindBy(xpath = "//*[text()='Accreditation Status COA']//following::label[1]")
	private WebElement AccreditationStatusCOA_TXT;

	@FindBy(xpath = "/html/body/div[7]/div/div[2]/div/div[3]/div/span[2]/div/button")
	private WebElement AccreditationStatusCHAPDone_btn;
	@FindBy(xpath = "//*[text()='Accreditation Status CHAP']//following::label[1]")
	private WebElement AccreditationStatusCHAP_TXT;

	@FindBy(xpath = "/html/body/div[7]/div/div[2]/div/div[3]/div/span[2]/div/button")
	private WebElement AccreditationStatusAAAHCDone_btn;
	@FindBy(xpath = "//*[text()='Accreditation Status AAAHC']//following::label[1]")
	private WebElement AccreditationStatusAAAHC_TXT;

	@FindBy(xpath = "/html/body/div[7]/div/div[2]/div/div[3]/div/span[2]/div/button")
	private WebElement AccreditationStatusCAHDone_btn;
	@FindBy(xpath = "//*[text()='Accreditation Status CAH']//following::label[1]")
	private WebElement AccreditationStatusCAH_TXT;

	@FindBy(xpath = "/html/body/div[7]/div/div[2]/div/div[3]/div/span[2]/div/button")
	private WebElement AccreditationStatusHFAPDone_btn;
	@FindBy(xpath = "//*[text()='Accreditation Status HFAP']//following::label[1]")
	private WebElement AccreditationStatusHFAP_TXT;

	@FindBy(xpath = "/html/body/div[7]/div/div[2]/div/div[3]/div/span[2]/div/button")
	private WebElement AccreditationStatusNIAHODone_btn;
	@FindBy(xpath = "Accreditation Status NIAHO']//following::label[1]")
	private WebElement AccreditationStatusNIAHO_TXT;

	@FindBy(xpath = "/html/body/div[7]/div/div[2]/div/div[3]/div/span[2]/div/button")
	private WebElement AccreditationStatusACHDone_btn;
	@FindBy(xpath = "//*[text()='Accreditation Status ACHC']//following::label[1]")
	private WebElement AccreditationStatusACHC_TXT;
	
	@FindBy(xpath = "//button[text()='Save & Continue']")
	private WebElement SaveCountinue_btn;

	@FindBy(xpath = "//*[@class='slds-button slds-button_icon slds-button_icon-brand']")
	private WebElement Check_btn;
	
	public void doaccredation() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Wait.waitUnitWebElementVisible(driver, TheJointCommission_TXT);
		ReusableMethods.javascriptclick(TheJointCommission_TXT, driver);
		Wait.waitUnitWebElementVisible(driver, TheJointCommission_1_TXT);
		ReusableMethods.javascriptclick(TheJointCommission_1_TXT, driver);
		TheJointCommission_1_TXT.sendKeys("Sep 8, 2020");
		ReusableMethods.javascriptclick(TheJointCommission_2_TXT, driver);
		TheJointCommission_2_TXT.sendKeys("Sep 8, 2022");

		/*CommissiononAccreditationofRehabilitationFacilities_TXT.click();
		Wait.waitUnitWebElementVisible(driver, CommissiononAccreditationofRehabilitationFacilities1_TXT);
		ReusableMethods.javascriptclick(CommissiononAccreditationofRehabilitationFacilities1_TXT, driver);
		CommissiononAccreditationofRehabilitationFacilities1_TXT.sendKeys("Sep 8, 2020");
		ReusableMethods.javascriptclick(CommissiononAccreditationofRehabilitationFacilities2_TXT, driver);
		CommissiononAccreditationofRehabilitationFacilities2_TXT.sendKeys("Sep 8, 2022");

		AmericanOsteopathicAssociation_2_TXT.click();
		Wait.waitUnitWebElementVisible(driver, AmericanOsteopathicAssociation_1_TXT);
		ReusableMethods.javascriptclick(AmericanOsteopathicAssociation_1_TXT, driver);
		AmericanOsteopathicAssociation_1_TXT.sendKeys("Sep 8, 2020");
		ReusableMethods.javascriptclick(AmericanOsteopathicAssociation_3_TXT, driver);
		AmericanOsteopathicAssociation_3_TXT.sendKeys("Sep 8, 2022");

		CouncilonAccreditation_2_TXT.click();
		Wait.waitUnitWebElementVisible(driver, CouncilonAccreditation__TXT);
		ReusableMethods.javascriptclick(CouncilonAccreditation__TXT, driver);
		CouncilonAccreditation__TXT.sendKeys("Sep 8, 2020");
		ReusableMethods.javascriptclick(CouncilonAccreditation_3_TXT, driver);
		CouncilonAccreditation_3_TXT.sendKeys("Sep 8, 2022");

		CommunityHealthAccreditationProgram_2_TXT.click();
		Wait.waitUnitWebElementVisible(driver, CommunityHealthAccreditationProgram__TXT);
		ReusableMethods.javascriptclick(CommunityHealthAccreditationProgram__TXT, driver);
		CommunityHealthAccreditationProgram__TXT.sendKeys("Sep 8, 2020");
		ReusableMethods.javascriptclick(CommunityHealthAccreditationProgram_3_TXT, driver);
		CommunityHealthAccreditationProgram_3_TXT.sendKeys("Sep 8, 2022");

		AmericanAssociationforAmbulatoryHealthCare_2_TXT.click();
		Wait.waitUnitWebElementVisible(driver, AmericanAssociationforAmbulatoryHealthCare__TXT);
		ReusableMethods.javascriptclick(AmericanAssociationforAmbulatoryHealthCare__TXT, driver);
		AmericanAssociationforAmbulatoryHealthCare__TXT.sendKeys("Sep 8, 2020");
		ReusableMethods.javascriptclick(AmericanAssociationforAmbulatoryHealthCare_3_TXT, driver);
		AmericanAssociationforAmbulatoryHealthCare_3_TXT.sendKeys("Sep 8, 2022");

		CriticalAccessHospitals_2_TXT.click();
		Wait.waitUnitWebElementVisible(driver, CriticalAccessHospitals__TXT);
		ReusableMethods.javascriptclick(CriticalAccessHospitals__TXT, driver);
		CriticalAccessHospitals__TXT.sendKeys("Sep 8, 2020");
		ReusableMethods.javascriptclick(CriticalAccessHospitals_3_TXT, driver);
		CriticalAccessHospitals_3_TXT.sendKeys("Sep 8, 2022");

		HealthcareFacilitiesAccreditationProgram_2_TXT.click();
		Wait.waitUnitWebElementVisible(driver, HealthcareFacilitiesAccreditationProgram__TXT);
		ReusableMethods.javascriptclick(HealthcareFacilitiesAccreditationProgram__TXT, driver);
		HealthcareFacilitiesAccreditationProgram__TXT.sendKeys("Sep 8, 2020");
		ReusableMethods.javascriptclick(HealthcareFacilitiesAccreditationProgram_3_TXT, driver);
		HealthcareFacilitiesAccreditationProgram_3_TXT.sendKeys("Sep 8, 2022");

		NationalIntegratedAccreditationforHealthcareOrganizations_2_TXT.click();
		/*
		 * Wait.waitUnitWebElementVisible(driver, TheJointCommission_1_TXT);
		 * ReusableMethods.javascriptclick(TheJointCommission_1_TXT, driver);
		 * TheJointCommission_1_TXT.sendKeys("Sep 8, 2020");
		 * ReusableMethods.javascriptclick(TheJointCommission_2_TXT, driver);
		 * TheJointCommission_2_TXT.sendKeys("Sep 8, 2022");
		 */

		/*AccreditationCommissionsforHealthcare_2_TXT.click();
		Wait.waitUnitWebElementVisible(driver, AccreditationCommissionsforHealthcare__TXT);
		ReusableMethods.javascriptclick(AccreditationCommissionsforHealthcare__TXT, driver);
		AccreditationCommissionsforHealthcare__TXT.sendKeys("Sep 8, 2020");
		ReusableMethods.javascriptclick(AccreditationCommissionsforHealthcare_3_TXT, driver);
		AccreditationCommissionsforHealthcare_3_TXT.sendKeys("Sep 8, 2022");

		ReusableMethods.javascriptclick(ASAMLevel3_TXT, driver);
		ReusableMethods.javascriptclick(ASAMLevel35_TXT, driver);
		ReusableMethods.javascriptclick(ASAMLevel37_TXT, driver);*/
				
ReusableMethods.javascriptclick(AccreditationStatusTheJointCommission_TXT, driver);
		
		ReusableMethods.uploadFile("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, AccreditationStatusTheJointCommissionDone_btn);
		ReusableMethods.javascriptclick(AccreditationStatusTheJointCommissionDone_btn, driver);
		
		
		
		/*ReusableMethods.javascriptclick(AccreditationStatusCARF_TXT, driver);
Thread.sleep(1500);
		ReusableMethods.uploadFile("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, AccreditationStatusCARFDone_btn);
		ReusableMethods.javascriptclick(AccreditationStatusCARFDone_btn, driver);
		
	
			Thread.sleep(1500);
		
		
ReusableMethods.javascriptclick(AccreditationStatusAOA_TXT, driver);
		
		ReusableMethods.uploadFile("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, AccreditationStatusAOADone_btn);
		ReusableMethods.javascriptclick(AccreditationStatusAOADone_btn, driver);
		Thread.sleep(1500);
		
		
ReusableMethods.javascriptclick(AccreditationStatusCOA_TXT, driver);
		
		ReusableMethods.uploadFile("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, AccreditationStatusCOADone_btn);
		ReusableMethods.javascriptclick(AccreditationStatusCOADone_btn, driver);
		
		Thread.sleep(1500);
		
ReusableMethods.javascriptclick(AccreditationStatusCHAP_TXT, driver);
		
		ReusableMethods.uploadFile("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, AccreditationStatusCHAPDone_btn);
		ReusableMethods.javascriptclick(AccreditationStatusCHAPDone_btn, driver);
		
		Thread.sleep(1500);
		
ReusableMethods.javascriptclick(AccreditationStatusAAAHC_TXT, driver);
		
		ReusableMethods.uploadFile("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, AccreditationStatusAAAHCDone_btn);
		ReusableMethods.javascriptclick(AccreditationStatusAAAHCDone_btn, driver);
		
		Thread.sleep(1500);
		
ReusableMethods.javascriptclick(AccreditationStatusCAH_TXT, driver);
		
		ReusableMethods.uploadFile("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, AccreditationStatusCAHDone_btn);
		ReusableMethods.javascriptclick(AccreditationStatusCAHDone_btn, driver);
		
		Thread.sleep(1500);
		
ReusableMethods.javascriptclick(AccreditationStatusHFAP_TXT, driver);
		
		ReusableMethods.uploadFile("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, AccreditationStatusHFAPDone_btn);
		ReusableMethods.javascriptclick(AccreditationStatusHFAPDone_btn, driver);
		
		Thread.sleep(1500);
		
ReusableMethods.javascriptclick(AccreditationStatusNIAHO_TXT, driver);
		
		ReusableMethods.uploadFile("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, AccreditationStatusNIAHODone_btn);
		ReusableMethods.javascriptclick(AccreditationStatusNIAHODone_btn, driver);
		Thread.sleep(1500);
		
		
ReusableMethods.javascriptclick(AccreditationStatusACHC_TXT, driver);
		
		ReusableMethods.uploadFile("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, AccreditationStatusACHDone_btn);
		ReusableMethods.javascriptclick(AccreditationStatusACHDone_btn, driver);
		
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
*/
		Wait.waitUnitWebElementVisible(driver, Check_btn);
		
		ReusableMethods.javascriptclick(SaveCountinue_btn, driver);
		

		}
}
