package FRNF.Pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import FNRFutil.ConfigFileReader;
import FNRFutil.ReusableMethods;
import FNRFutil.Wait;

public class legalStatus {
	WebDriver driver;
	ConfigFileReader configFileReader = null;
	ReusableMethods ReusableMethods = null;

	public legalStatus(WebDriver driver) {
		this.driver = driver;
		configFileReader = new ConfigFileReader();
		ReusableMethods = new ReusableMethods();
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//*[text()='Legal Status']//following::button[1]")
	private WebElement LegalStatus_SEL;
	
	
	
	@FindBy(xpath = "//*[text()='Please Provide A Brief Explanation For Each Incident']//following::input[1]")
	private WebElement Incident;
	
	@FindBy(xpath = "//a[text()='Legal Status ']")
	private WebElement Locations_lnk;
	
	@FindBy(xpath = "//button[text()='Save & Continue']")
	private WebElement SaveCountinue_btn;
	
	public void dolegal() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Wait.waitUnitWebElementVisible(driver, Locations_lnk);
		ReusableMethods.javascriptclick(Locations_lnk, driver);
		Wait.untilPageLoadComplete(driver);
		
		
		
		
		
		LegalStatus_SEL.sendKeys("No");

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		LegalStatus_SEL.sendKeys(Keys.ENTER);
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LegalStatus_SEL.sendKeys("No");
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ReusableMethods.javascriptclick(LegalStatus_SEL, driver);
		//ReusableMethods.clickdropdown(1);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		ReusableMethods.javascriptclick(SaveCountinue_btn, driver);
	}
}
