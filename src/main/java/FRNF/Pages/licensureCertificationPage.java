package FRNF.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import FNRFutil.ConfigFileReader;
import FNRFutil.ReusableMethods;
import FNRFutil.Wait;

public class licensureCertificationPage {
	WebDriver driver;
	ConfigFileReader configFileReader = null;
	ReusableMethods ReusableMethods = null;

	public licensureCertificationPage(WebDriver driver) {
		this.driver = driver;
		configFileReader = new ConfigFileReader();
		ReusableMethods = new ReusableMethods();
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//*[text()='Does your State require a license for the levels of care you are providing?']//following::button[1]")
	private WebElement License_SEL;
	

	@FindBy(xpath = "//*[text()='Yes']")
	private WebElement No_btn;
	
	@FindBy(xpath = "//*[text()='Does the Organizational provider state license/certification include a site visit by the State?']//following::button[1]")
	private WebElement stateLicense_SEL;
	
	@FindBy(xpath = "//*[text()='Yes']")
	private WebElement State_Yes_btn;
	
	@FindBy(xpath = "//*[text()='Is your facility a CMS approved facility?']//following::button[1]")
	private WebElement facilityLicense_SEL;
	
	@FindBy(xpath = "//*[text()='Yes']")
	private WebElement facility_Yes_btn;
	
	@FindBy(xpath = "//*[text()='Do you hold a CLIA (Clinical Laboratory Improvement Amendments) waiver?']//following::button[1]")
	private WebElement CLIA;
	
	@FindBy(xpath = "//*[text()='Yes']")
	private WebElement CLIA_Yes_btn;
	
	@FindBy(xpath = "//button[text()='Add License']")
	private WebElement AddLicense_btn;
	
	@FindBy(xpath = "//*[text()=' Entity Issuing License or Certification']//following::input[1]")
	private WebElement EntityIssuing_TXT;
	
	@FindBy(xpath = "//*[text()=' Entity Issuing License or Certification']//following::input[2]")
	private WebElement Type_TXT;
	
	@FindBy(xpath = "//*[text()=' Entity Issuing License or Certification']//following::input[3]")
	private WebElement License_TXT;
	
	@FindBy(xpath = "//*[text()=' Entity Issuing License or Certification']//following::input[4]")
	private WebElement Expiration_TXT;
	
	@FindBy(xpath = "//button[text()='Save & Continue']")
	private WebElement SaveCountinue_btn;
	
	@FindBy(xpath = "//*[text()='Current State License(s)/ Certificate(s) for all behavioral health services you provide']//following::label[1]")
	private WebElement Current_Browse_btn;
	
	@FindBy(xpath = "//*[text()='Controlled Substance DEA for Facility']//following::label")
	private WebElement DEABrowse_btn;
	
	@FindBy(xpath = "//*[text()='Substance Abuse License or Opiod Abuse License for Facility']//following::label")
	private WebElement OpiodBrowse_btn;
	
	@FindBy(xpath = "//*[text()='Controlled Substance DEA for Individual Provider(s)']//following::label")
	private WebElement subdeaBrowse_btn;
	
	@FindBy(xpath = "//*[text()='Completed State Site Audit']//following::label")
	private WebElement AuditBrowse_btn;
	
	@FindBy(xpath = "//*[text()='CLIA Waiver']//following::label")
	private WebElement CLIABrowse_btn;
	
	@FindBy(xpath = "/html/body/div[7]/div/div[2]/div/div[3]/div/span[2]/div/button")
	private WebElement Done_btn1;
	
	@FindBy(xpath = "/html/body/div[7]/div/div[2]/div/div[3]/div/span[2]/div/button")
	private WebElement Done_btn2;
	
	@FindBy(xpath = "/html/body/div[7]/div/div[2]/div/div[3]/div/span[2]/div/button")
	private WebElement Done_btn3;
	
	@FindBy(xpath = "/html/body/div[7]/div/div[2]/div/div[3]/div/span[2]/div/button")
	private WebElement Done_btn4;
	
	@FindBy(xpath = "/html/body/div[7]/div/div[2]/div/div[3]/div/span[2]/div/button")
	private WebElement Done_btn5;
	
	@FindBy(xpath = "//a[text()='Licensure / Certification']")
	private WebElement Locations_lnk;
	
	@FindBy(xpath = "//*[text()='Completed State Site Audit']")
	private WebElement Check;
	
	public void setcontractorstatus() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Wait.waitUnitWebElementVisible(driver, Locations_lnk);
		ReusableMethods.javascriptclick(Locations_lnk, driver);
		Wait.waitUnitWebElementVisible(driver, License_SEL);
		ReusableMethods.javascriptclick(License_SEL, driver);
		Wait.waitUnitWebElementVisible(driver, No_btn);
		ReusableMethods.javascriptclick(No_btn, driver);
		
		Wait.waitUnitWebElementVisible(driver, stateLicense_SEL);
		ReusableMethods.javascriptclick(stateLicense_SEL, driver);
		ReusableMethods.clickdropdown(0);
		
		Wait.waitUnitWebElementVisible(driver, License_SEL);
		ReusableMethods.javascriptclick(License_SEL, driver);
		Wait.waitUnitWebElementVisible(driver, No_btn);
		ReusableMethods.javascriptclick(No_btn, driver);
		
		
		ReusableMethods.javascriptclick(facilityLicense_SEL, driver);
		ReusableMethods.clickdropdown(0);
		
		ReusableMethods.javascriptclick(CLIA, driver);
		ReusableMethods.clickdropdown(0);
		
		ReusableMethods.javascriptclick(AddLicense_btn, driver);
		Wait.waitUnitWebElementVisible(driver, EntityIssuing_TXT);
		EntityIssuing_TXT.sendKeys("Test");
		Type_TXT.sendKeys("Test");
		License_TXT.sendKeys("MD103");
		ReusableMethods.javascriptclick(Expiration_TXT, driver);
		Expiration_TXT.sendKeys("Sep 8, 2022");
		
		
		
		ReusableMethods.javascriptclick(Current_Browse_btn, driver);
		
		ReusableMethods.uploadFile("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, Done_btn1);
		ReusableMethods.javascriptclick(Done_btn1, driver);
		
		ReusableMethods.javascriptclick(DEABrowse_btn, driver);
		
		ReusableMethods.uploadFile("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, Done_btn1);
		ReusableMethods.javascriptclick(Done_btn2, driver);
		
		ReusableMethods.javascriptclick(OpiodBrowse_btn, driver);
		
		ReusableMethods.uploadFile("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, Done_btn1);
		ReusableMethods.javascriptclick(Done_btn3, driver);
		
		ReusableMethods.javascriptclick(subdeaBrowse_btn, driver);
		
		ReusableMethods.uploadFile("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, Done_btn1);
		ReusableMethods.javascriptclick(Done_btn4, driver);
		
        ReusableMethods.javascriptclick(CLIABrowse_btn, driver);
		
		ReusableMethods.uploadFile("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, Done_btn1);
		ReusableMethods.javascriptclick(Done_btn4, driver);
		
		ReusableMethods.javascriptclick(AuditBrowse_btn, driver);
		
		ReusableMethods.uploadFile("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		Wait.waitUnitWebElementClickable(driver, Done_btn1);
		ReusableMethods.javascriptclick(Done_btn5, driver);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Wait.waitUnitWebElementVisible(driver, Check);
		ReusableMethods.javascriptclick(SaveCountinue_btn, driver);
		
	}
}
