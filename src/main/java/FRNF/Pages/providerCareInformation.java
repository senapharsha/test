package FRNF.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import FNRFutil.ConfigFileReader;
import FNRFutil.ReusableMethods;
import FNRFutil.Wait;

public class providerCareInformation {

	WebDriver driver;
	ConfigFileReader configFileReader = null;
	ReusableMethods ReusableMethods = null;

	public providerCareInformation(WebDriver driver) {
		this.driver = driver;
		configFileReader = new ConfigFileReader();
		ReusableMethods = new ReusableMethods();
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath = "//*[text()='Primary Contact']//following::input[1]")
	private WebElement PrimaryContact_TXT;
	
	@FindBy(xpath = "//*[text()='Primary Contact']//following::input[2]")
	private WebElement PrimaryContact_TXT_1;
	
	@FindBy(xpath = "//*[text()='Primary Contact']//following::input[3]")
	private WebElement PrimaryContact_TXT_2;

@FindBy(xpath = "//*[text()='Signatory Contact']//following::input[1]")
	private WebElement SignatoryContact_TXT;
	
	@FindBy(xpath = "//*[text()='Signatory Contact']//following::input[2]")
	private WebElement SignatoryContact_TXT_2;
	
	@FindBy(xpath = "//*[text()='Signatory Contact']//following::input[3]")
	private WebElement SignatoryContact_TXT_3;
	
	@FindBy(xpath = "//*[text()='Facility Contracting Contact']//following::input[1]")
	private WebElement FacilityContractingContact_TXT;
	
	@FindBy(xpath = "//*[text()='Facility Contracting Contact']//following::input[2]")
	private WebElement FacilityContractingContact_TXT_1;
	
	@FindBy(xpath = "//*[text()='Facility Contracting Contact']//following::input[3]")
	private WebElement FacilityContractingContact_TXT_2;
	
	@FindBy(xpath = "//*[text()='Administrator / Roster Contact']//following::input[1]")
	private WebElement AdministratorRosterContact_TXT;
	
	@FindBy(xpath = "//*[text()='Administrator / Roster Contact']//following::input[2]")
	private WebElement AdministratorRosterContact_TXT_2;
	
	@FindBy(xpath = "//*[text()='Administrator / Roster Contact']//following::input[3]")
	private WebElement AdministratorRosterContact_TXT_3;
	
	@FindBy(xpath = "//*[text()='Business Office Manager']//following::input[1]")
	private WebElement BusinessOfficeManager_TXT;
	
	@FindBy(xpath = "//*[text()='Business Office Manager']//following::input[2]")
	private WebElement BusinessOfficeManager_TXT_2;
	
	@FindBy(xpath = "//*[text()='Business Office Manager']//following::input[3]")
	private WebElement BusinessOfficeManager_TXT_3;
	
	@FindBy(xpath = "//*[text()='Director of Clinical Services']//following::input[1]")
	private WebElement DirectorofClinicalServices_TXT;
	
	@FindBy(xpath = "//*[text()='Director of Clinical Services']//following::input[2]")
	private WebElement DirectorofClinicalServices_TXT_2;
	
	@FindBy(xpath = "//*[text()='Director of Clinical Services']//following::input[3]")
	private WebElement DirectorofClinicalServices_TXT_3;
	
	@FindBy(xpath = "//*[text()='Medical Director']//following::input[1]")
	private WebElement MedicalDirector_TXT;
	
	@FindBy(xpath = "//*[text()='Medical Director']//following::input[2]")
	private WebElement MedicalDirector_TXT_2;
	
	@FindBy(xpath = "//*[text()='Medical Director']//following::input[3]")
	private WebElement MedicalDirector_TXT_3;
	
	@FindBy(xpath = "//*[text()='Chief Executive Officer']//following::input[1]")
	private WebElement ChiefExecutiveOfficer_TXT;
	
	@FindBy(xpath = "//*[text()='Chief Executive Officer']//following::input[2]")
	private WebElement ChiefExecutiveOfficer_TXT_2;
	
	@FindBy(xpath = "//*[text()='Chief Executive Officer']//following::input[3]")
	private WebElement ChiefExecutiveOfficer_TXT_3;
	
	@FindBy(xpath = "//button[text()='Save & Continue']")
	private WebElement SaveCountinue_btn;
	
	@FindBy(xpath = "//button[text()='Save & Continue']")
	private WebElement SaveCountinue_btn2;
	
	
	
	@FindBy(xpath = "//a[text()='Staffing']")
	private WebElement staffing_lnk;
	
	@FindBy(xpath = "//a[text()='Practice Location(s)']")
	private WebElement Locations_lnk;
	
	
	
	@FindBy(xpath = "//a[text()='Accreditation']")
	private WebElement Accreditation_lnk;
	public void createprovdercareinformation() {
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Wait.waitUnitWebElementVisible(driver, PrimaryContact_TXT);
		PrimaryContact_TXT.sendKeys("Anil Kumar");
		PrimaryContact_TXT_1.sendKeys("(939)056-3471");
		PrimaryContact_TXT_2.sendKeys("anil_k_vykuntham@optum.com");
		SignatoryContact_TXT.sendKeys("Jonathan");
		SignatoryContact_TXT_2.sendKeys("(939)056-3471");
		SignatoryContact_TXT_3.sendKeys("anil_k_vykuntham@optum.com");
		FacilityContractingContact_TXT.sendKeys("Test");
		FacilityContractingContact_TXT_1.sendKeys("(939)056-3471");
		FacilityContractingContact_TXT_2.sendKeys("Test@gmail.com");
		AdministratorRosterContact_TXT.sendKeys("Test");
		AdministratorRosterContact_TXT_2.sendKeys("(939)056-3471");
		AdministratorRosterContact_TXT_3.sendKeys("Test@gmail.com");
		BusinessOfficeManager_TXT.sendKeys("Test");
		BusinessOfficeManager_TXT_2.sendKeys("(939)056-3471");
		BusinessOfficeManager_TXT_3.sendKeys("Test@gmail.com");
		DirectorofClinicalServices_TXT.sendKeys("Test");
		DirectorofClinicalServices_TXT_2.sendKeys("(939)056-3471");
		DirectorofClinicalServices_TXT_3.sendKeys("Test@gmail.com");
		MedicalDirector_TXT.sendKeys("Test");
		MedicalDirector_TXT_2.sendKeys("(939)056-3471");
		MedicalDirector_TXT_3.sendKeys("Test@gmail.com");
		ChiefExecutiveOfficer_TXT.sendKeys("Test");
		ChiefExecutiveOfficer_TXT_2.sendKeys("(939)056-3471");
		ChiefExecutiveOfficer_TXT_3.sendKeys("Test@gmail.com");
		
		ReusableMethods.javascriptclick(SaveCountinue_btn, driver);
		
//		try {
//			Thread.sleep(5000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		//ReusableMethods.click(staffing_lnk, "staffing", driver);
	}
	
	public void setloactions() {
		try {
			Thread.sleep(2500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ReusableMethods.javascriptclick(Locations_lnk, driver);
		Wait.waitUnitWebElementVisible(driver, SaveCountinue_btn2);
		ReusableMethods.javascriptclick(SaveCountinue_btn2, driver);
		
		try {
			Thread.sleep(2500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setAccreditation() {
		try {
			Thread.sleep(2500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ReusableMethods.javascriptclick(Accreditation_lnk, driver);
		
	}
}
