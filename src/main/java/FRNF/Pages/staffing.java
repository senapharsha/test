package FRNF.Pages;

import java.util.List;

import org.apache.poi.util.SystemOutLogger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import FNRFutil.ConfigFileReader;
import FNRFutil.ReusableMethods;
import FNRFutil.Wait;

public class staffing {

	WebDriver driver;
	ConfigFileReader configFileReader = null;
	ReusableMethods ReusableMethods = null;

	public staffing(WebDriver driver) {
		this.driver = driver;
		configFileReader = new ConfigFileReader();
		ReusableMethods = new ReusableMethods();
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//*[text()='Number of board certified psychiatrists on staff']//preceding::button[1]")
	private WebElement License_SEL;

	@FindBy(xpath = "//*[text()='No']")
	private WebElement No_btn;

	@FindBy(xpath = "//*[text()='Number of board certified psychiatrists on staff']//following::input[@type='text']")
	private List<WebElement> NoOfVisits;
	
//	@FindBy(xpath = "//*[contains(text(),'NUMBER OF VISITS BY MD')]//following::input")
//	private List<WebElement> NoOfVisits;

	@FindBy(xpath = "//*[text()='IP Acute']//following::input[1]")
	private WebElement IPAcute1;

	@FindBy(xpath = "//*[text()='IP Acute']//following::input[1]")
	private WebElement IPAcute2;

	@FindBy(xpath = "//*[text()='Number of IP Acute visits required in Facility bylaws or policy per week']//following::input[1]")
	private WebElement policyperweek;

	@FindBy(xpath = "//*[text()='Number of IP Acute visits required in Facility bylaws or policy per week']//following::input[1]")
	private WebElement policyperweek2;

	@FindBy(xpath = "//*[text()='Medically Managed Intensive Inpatient Services ASAM 4']//following::input[1]")
	private WebElement ASAM;

	@FindBy(xpath = "//*[text()='Medically Managed Intensive Inpatient Services ASAM 4']//following::input[1]")
	private WebElement ASAM1;

	@FindBy(xpath = "//*[text()='Number of Psychiatrist visits per week for Medically Managed Intensive Inpatient Services ASAM 4']//following::input[1]")
	private WebElement ServicesASAM4;

	@FindBy(xpath = "//*[text()='Number of Psychiatrist visits per week for Medically Managed Intensive Inpatient Services ASAM 4']//following::input[1]")
	private WebElement ServicesASAM4_1;

	@FindBy(xpath = "//*[text()='Number of Medically Managed Intensive Inpatient Services ASAM 4 visits required in Facility bylaws or policy per week']//following::input[1]")
	private WebElement MedicallyManagedIntensive;

	@FindBy(xpath = "//*[text()='Number of Medically Managed Intensive Inpatient Services ASAM 4 visits required in Facility bylaws or policy per week']//following::input[1]")
	private WebElement MedicallyManagedIntensive2;

	@FindBy(xpath = "//*[text()='Medically Monitored Intensive Inpatient Services ASAM 3.7 WM']//following::input[1]")
	private WebElement ServicesASAM;

	@FindBy(xpath = "//*[text()='Medically Monitored Intensive Inpatient Services ASAM 3.7 WM']//following::input[1]")
	private WebElement ServicesASAM2;

	@FindBy(xpath = "//*[text()='SUD Residential ASAM 3.7']//following::input[1]")
	private WebElement ASAM37;

	@FindBy(xpath = "//*[text()='SUD Residential ASAM 3.7']//following::input[1]")
	private WebElement ASAM37_1;

	@FindBy(xpath = "//*[text()='MH Residential']//following::input[1]")
	private WebElement MHResidential;

	@FindBy(xpath = "//*[text()='MH Residential']//following::input[1]")
	private WebElement MHResidential_1;

	@FindBy(xpath = "//*[text()='Partial Hospitalization ASAM 2.5']//following::input[1]")
	private WebElement HospitalizationASAM;

	@FindBy(xpath = "//*[text()='Partial Hospitalization ASAM 2.5']//following::input[1]")
	private WebElement HospitalizationASAM1;

	@FindBy(xpath = "//*[text()='MH PHP']//following::input[1]")
	private WebElement MHPHP;

	@FindBy(xpath = "//*[text()='MH PHP']//following::input[1]")
	private WebElement MHPHP1;

	@FindBy(xpath = "//*[text()='Intensive Outpatient Services ASAM 2.1']//following::input[1]")
	private WebElement OutpatientServices;

	@FindBy(xpath = "//*[text()='Intensive Outpatient Services ASAM 2.1']//following::input[1]")
	private WebElement OutpatientServices1;

	@FindBy(xpath = "//*[text()='MH IOP']//following::input[1]")
	private WebElement MHIOP;

	@FindBy(xpath = "//*[text()='MH IOP']//following::input[1]")
	private WebElement MHIOP1;

	@FindBy(xpath = "/html/body/div[7]/div/div[2]/div/div[3]/div/span[2]/div/button")
	private WebElement AccreditationStatusTheJointCommissionDone_btn;
	
	@FindBy(xpath = "//*[text()='Staff Roster for all behavioral health staff involved with your programs.']//following::label[1]")
	private WebElement staffRoaster_TXT;

	@FindBy(xpath = "//button[text()='Save & Continue']")
	private WebElement SaveCountinue_btn;

	@FindBy(xpath = "//*[@class='slds-button slds-button_icon slds-button_icon-brand']")
	private WebElement Check_btn;

	public void setcontractorstatus() {
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Wait.waitUnitWebElementVisible(driver, License_SEL);
		ReusableMethods.javascriptclick(License_SEL, driver);
		Wait.waitUnitWebElementVisible(driver, No_btn);
		ReusableMethods.javascriptclick(No_btn, driver);
		//certifiedpsychiatrists.sendKeys("1");
		
//		IPAcute1.sendKeys("1");
//		IPAcute2.sendKeys("1");
//		policyperweek.sendKeys("1");
//		policyperweek2.sendKeys("1");
//		ASAM.sendKeys("1");
//		ASAM1.sendKeys("1");
//		ServicesASAM4.sendKeys("1");
//		ServicesASAM4_1.sendKeys("1");
//		MedicallyManagedIntensive.sendKeys("1");
//		MedicallyManagedIntensive2.sendKeys("1");
//		ServicesASAM.sendKeys("1");
//		ServicesASAM2.sendKeys("1");
//		ASAM37.sendKeys("1");
//		ASAM37_1.sendKeys("1");
//		MHResidential.sendKeys("1");
//		MHResidential_1.sendKeys("1");
//		HospitalizationASAM.sendKeys("1");
//		HospitalizationASAM1.sendKeys("1");
//		MHPHP.sendKeys("1");
//		MHPHP1.sendKeys("1");
//		OutpatientServices.sendKeys("1");
//		OutpatientServices1.sendKeys("1");
//		MHIOP.sendKeys("1");
//		MHIOP1.sendKeys("1");

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		NoOfVisits=driver.findElements(By.xpath("//*[text()='Number of board certified psychiatrists on staff']//following::input[@type='text']"));
		
		int eles = NoOfVisits.size();
		System.out.println(eles);
		for (int i = 0; i < eles; i++) {
			WebElement ele = NoOfVisits.get(i);
			System.out.println(ele);
			ele.sendKeys("1");
		

	}
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Uploading file.........");
		ReusableMethods.javascriptclick(staffRoaster_TXT, driver);

		ReusableMethods.uploadFileForStaffing("C:\\Users\\Anil Vykuntam\\Desktop\\Docs\\W9 form.pdf");
		
		
		Wait.waitUnitWebElementClickable(driver, AccreditationStatusTheJointCommissionDone_btn);
		ReusableMethods.javascriptclick(AccreditationStatusTheJointCommissionDone_btn, driver);
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Wait.waitUnitWebElementVisible(driver, Check_btn);

		ReusableMethods.javascriptclick(SaveCountinue_btn, driver);

	}
}
