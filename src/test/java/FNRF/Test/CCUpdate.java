package FNRF.Test;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import FNRFutil.ConfigFileReader;
import FNRFutil.ReusableMethods;
import FNRFutil.Wait;
import FNRFutil.WebDriverManager;
import FRNF.Pages.CredCycleUpdate;
import FRNF.Pages.LandingPage;




public class CCUpdate {
	
	ConfigFileReader configFileReader = null;
	WebDriverManager webDriverManager = null;
	WebDriver driver = null;
	ReusableMethods reusableMethods = null;
	LandingPage Landing = null;
	CredCycleUpdate cc = null;
	
	
	@BeforeClass()
	public void initWebDriver() throws IOException {
		webDriverManager = new WebDriverManager();
		configFileReader = new ConfigFileReader();
		driver = webDriverManager.getDriver();
		Landing = new LandingPage(driver);
		
		Landing.naviageSFURL();
		
		Wait.untilPageLoadComplete(driver);
		Landing.logIn();
		Landing.naviageCredURL(LandingPage.cred);
		reusableMethods = new ReusableMethods();
		
		cc=new CredCycleUpdate(driver);
	}
	
	@Test
	public void CredUpd() {
		
		cc.CredUpdate(LandingPage.state);
		
	}
}
