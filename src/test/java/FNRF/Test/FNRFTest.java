package FNRF.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import FNRFutil.ConfigFileReader;
import FNRFutil.ReusableMethods;
import FNRFutil.Wait;
import FNRFutil.WebDriverManager;
import FRNF.Pages.Compensation;
import FRNF.Pages.ContractorPage;
import FRNF.Pages.IdentificationInformation;
import FRNF.Pages.LandingPage;
import FRNF.Pages.LevelsofCare;
import FRNF.Pages.PracticeLocations;
import FRNF.Pages.providerCareInformation;
import FRNF.Pages.accredation;
import FRNF.Pages.licensureCertificationPage;
import FRNF.Pages.MedicareMedicade;
import FRNF.Pages.staffing;
import FRNF.Pages.Deliveryofcare;
import FRNF.Pages.legalStatus;
import FRNF.Pages.GeneralProfessionalLiability;
import FRNF.Pages.LOCATIONACCESSIBILITIES;
import FRNF.Pages.FACILITYTYPEINFORMATION;
import FRNF.Pages.ServiceDeliverySpecialtyServices;
import FRNF.Pages.AcknowledgeandCertify;


public class FNRFTest {
	ConfigFileReader configFileReader = null;
	WebDriverManager webDriverManager = null;
	WebDriver driver = null;
	ReusableMethods reusableMethods = null;
	LandingPage Landing = null;
	ContractorPage Contract = null;
	IdentificationInformation Idi = null;
	LevelsofCare level = null;
	Compensation compensation = null;
	PracticeLocations practiceLocations = null;
	providerCareInformation providerCareInformation = null;
	accredation accredation = null;
	licensureCertificationPage licensureCertificationPage = null;
	MedicareMedicade MedicareMedicade = null;
	staffing staffing = null;
	Deliveryofcare Deliveryofcare = null;
	legalStatus legalStatus = null;
	GeneralProfessionalLiability general = null;
	LOCATIONACCESSIBILITIES location = null;
	FACILITYTYPEINFORMATION facilityinformation = null;
	ServiceDeliverySpecialtyServices service = null;
	AcknowledgeandCertify accept = null;
	private final static String SheetName = "IdentificationInformation";
	

	@BeforeClass()
	public void initWebDriver() throws IOException {
		webDriverManager = new WebDriverManager();
		configFileReader = new ConfigFileReader();
		driver = webDriverManager.getDriver();
		Landing = new LandingPage(driver);
		Contract = new ContractorPage(driver);
		Idi = new IdentificationInformation(driver);
		Landing.naviageURL();
		Wait.untilPageLoadComplete(driver);
		reusableMethods = new ReusableMethods();
		level = new LevelsofCare(driver);
		compensation = new Compensation(driver);
		practiceLocations = new PracticeLocations(driver);
		providerCareInformation = new providerCareInformation(driver);
		accredation = new accredation(driver);
		licensureCertificationPage = new licensureCertificationPage(driver);
		MedicareMedicade = new MedicareMedicade(driver);
		staffing = new staffing(driver);
		Deliveryofcare = new Deliveryofcare(driver);
		legalStatus = new legalStatus(driver);
		general = new GeneralProfessionalLiability(driver);
		location = new LOCATIONACCESSIBILITIES(driver);
		facilityinformation = new FACILITYTYPEINFORMATION(driver);
		service = new ServiceDeliverySpecialtyServices(driver);
		accept = new AcknowledgeandCertify(driver);

	}

	@Test
	public void FRNF() {
		ArrayList<Integer> list = reusableMethods.getrow("NY", SheetName); //Mention the practicing state here
		
		for (int i = 0; i < list.size(); i++) {
			Map<String, String> xl = reusableMethods.getInputData(SheetName, list.get(i));
			Landing.dosearch(xl);
			Wait.untilPageLoadComplete(driver);
			Contract.setcontractorstatus(xl);
			Wait.untilPageLoadComplete(driver);
			Idi.fillIdentificationInformation(xl);
			level.setcontractorstatus(xl);
				compensation.compenstion();
			practiceLocations.Addlocation(xl);
			providerCareInformation.createprovdercareinformation();
			providerCareInformation.setloactions();
			providerCareInformation.setAccreditation();
			accredation.doaccredation();
			licensureCertificationPage.setcontractorstatus();
			MedicareMedicade.doentermedicaremedicade();
			staffing.setcontractorstatus();
			Deliveryofcare.doentercaredetails();
			service.enterservicedelivery();
			legalStatus.dolegal();
			general.doenterdetails();
			location.addlocationaccessbility();
			facilityinformation.enterfacilityinformation();
			accept.doaccept();
		}

	}

	@DataProvider(name = "data-provider03")
	public Object[][] dataProviderMethod03() {
		reusableMethods = new ReusableMethods();
		return reusableMethods.getData("IdentificationInformation");

	}

	/*
	 * @AfterClass() public void After() { driver.quit(); driver.close();
	 * 
	 * }
	 */

}
