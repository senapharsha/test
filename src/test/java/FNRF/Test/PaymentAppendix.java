package FNRF.Test;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import FNRFutil.ConfigFileReader;
import FNRFutil.ReusableMethods;
import FNRFutil.Wait;
import FNRFutil.WebDriverManager;
import FRNF.Pages.CredCycleUpdate;
import FRNF.Pages.LandingPage;
import FRNF.Pages.PaymentAppendixFacility;


public class PaymentAppendix {
	

	ConfigFileReader configFileReader = null;
	WebDriverManager webDriverManager = null;
	WebDriver driver = null;
	ReusableMethods reusableMethods = null;
	LandingPage Landing = null;
	PaymentAppendixFacility pay = null;
	
	
	@BeforeClass()
	public void initWebDriver() throws IOException {
		webDriverManager = new WebDriverManager();
		configFileReader = new ConfigFileReader();
		driver = webDriverManager.getDriver();
		Landing = new LandingPage(driver);
		Landing.naviageSFURL();
		
		Wait.untilPageLoadComplete(driver);
		Landing.logIn();
		Landing.naviageCredURL(LandingPage.cred);
		
//		driver.get(LandingPage.ulink);
//		Wait.untilPageLoadComplete(driver);
		
		reusableMethods = new ReusableMethods();
		
		pay=new PaymentAppendixFacility(driver);
		pay.clickPaymentAppendix();
	}
	
	@Test
	public void SavePaymentAppendixData() {
		
		// State 'MO', 'KS', 'WA' completed		// State 'NE' completed		// State 'KY' completed  // State 'WI' completed // UT completed //NY done
		
		
		
		pay.FillPaymentAppendix();
		
	}

}
